CREATE DATABASE IF NOT EXISTS codeinblocks;

use codeinblocks;

CREATE TABLE USER_PROFILES (
  user_ID       varchar(15)      not null,
  fname         varchar(50)      not null,
  lname         varchar(50)      not null,
  username      varchar(50)      not null,
  password      text             not null,
  email         varchar(100)     not null,

  constraint primary key (user_ID)
);

CREATE TABLE WORKSHEETS (
  sheet_ref         varchar(15)      not null,
  sheet_num         int              not null,
  title             varchar(200)     not null,
  instructions      varchar(800),

  constraint primary key (sheet_num)
);

CREATE TABLE SESSIONS (
  sheet_num         int              not null,
  user_ID           varchar(15)      not null,
  score             int              default 0,

  constraint foreign key (sheet_num) REFERENCES WORKSHEETS (sheet_num),
  constraint foreign key (user_ID) REFERENCES USER_PROFILES (user_ID)
);

CREATE TABLE TOPICS (
  topic_num       int               not null,
  title           varchar(100)      not null,
  description     text,
  cover_path      text,
  sheet_after     boolean           default false,
  sheet_num       int               not null,

  constraint primary key (topic_num),
  constraint foreign key (sheet_num) REFERENCES WORKSHEETS (sheet_num)
);

CREATE TABLE PROGRESS (
  user_ID       varchar(15)      not null,
  topic_num     int              not null,
  curr_slide    int              default 0,
  percentage    float            default 0.0,
  notes         varchar(800),

  constraint primary key (user_ID, topic_num),
  constraint foreign key (user_ID) REFERENCES USER_PROFILES (user_ID),
  constraint foreign key (topic_num) REFERENCES TOPICS (topic_num)
);

CREATE TABLE SLIDES (
  slide_ID              varchar(15)      not null,
  topic_num             int              not null,
  slide_num             int              not null,
  file_path             varchar(100)     not null,
  is_practice_slide     boolean          not null,
  practice_key          text,

  constraint primary key (slide_ID),
  constraint foreign key (topic_num) REFERENCES TOPICS (topic_num)
);

CREATE TABLE QUESTIONS (
  question_ID        varchar(15)      not null,
  sheet_num          int              not null,
  question           text             not null,
  option_A           text,
  option_B           text,
  option_C           text,
  Answer             varchar(500)       not null,

  constraint primary key (question_ID),
  constraint foreign key (sheet_num) REFERENCES WORKSHEETS (sheet_num)
);

CREATE TABLE ACTIVITIES (
  activity_ID          varchar(8)      not null,
  title                varchar(60)     not null,
  description          text            not null,

  constraint primary key (activity_ID)
);

CREATE TABLE ACTIVITY_LOGS (
  date_time          datetime         DEFAULT CURRENT_TIMESTAMP,
  user_ID            varchar(15)      not null,
  activity_ID        varchar(8)       not null,

  constraint foreign key (user_ID) REFERENCES USER_PROFILES (user_ID),
  constraint foreign key (activity_ID) REFERENCES ACTIVITIES (activity_ID)
);


INSERT INTO USER_PROFILES VALUES
("5d01dabf6e58c", "Greg", "Basera", "GregBasera", "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8", "gbasera@gbox.adnu.edu.ph");

INSERT INTO WORKSHEETS VALUES
('5dd943d88d5e4', 1, "The Basics of Programming and Variables", null),
('5dd943d88d5e7', 2, "Arithmetic, Relational Operators and Conditional Statements", null),
('5dd943d88d5e8', 3, "Logical Operators and Iterative Statements", null);

INSERT INTO SESSIONS VALUES
(1, "5d01dabf6e58c", 0),
(2, "5d01dabf6e58c", 0),
(3, "5d01dabf6e58c", 0);

INSERT INTO TOPICS VALUES
(1, "The Basics of Programming", "", "assets/imgs/topic_imgs/Topic-1.png", false, 1),
(2, "Variables", "", "assets/imgs/topic_imgs/Topic-2.png", true, 1),
(3, "Arithmetic Operators", "", "assets/imgs/topic_imgs/Topic-3.png", false, 2),
(4, "Relational Operators", "", "assets/imgs/topic_imgs/Topic-4.png", false, 2),
(5, "Conditional Statements", "", "assets/imgs/topic_imgs/Topic-5.png", true, 2),
(6, "Logical Operators", "", "assets/imgs/topic_imgs/Topic-6.png", false, 3),
(7, "Iterative Statements", "", "assets/imgs/topic_imgs/Topic-7.png", true, 3);

INSERT INTO PROGRESS VALUES
("5d01dabf6e58c", 1, 1, 0.0, "Notes for first topic"),
("5d01dabf6e58c", 2, 0, 0.0, null),
("5d01dabf6e58c", 3, 0, 0.0, null),
("5d01dabf6e58c", 4, 0, 0.0, null),
("5d01dabf6e58c", 5, 0, 0.0, null),
("5d01dabf6e58c", 6, 0, 0.0, null),
("5d01dabf6e58c", 7, 0, 0.0, null);

INSERT INTO SLIDES VALUES
-- Basics
("5d0297bf04c2e", 1, 1, "assets/imgs/slides/topic1/Slide1.PNG", false, null),
("5d0297f8a9823", 1, 2, "assets/imgs/slides/topic1/Slide2.PNG", false, null),
("5d02983293de8", 1, 3, "assets/imgs/slides/topic1/Slide3.PNG", false, null),
("5d029846a52f4", 1, 4, "assets/imgs/slides/topic1/Slide4.PNG", false, null),
("5d029857d2e5d", 1, 5, "assets/imgs/slides/topic1/Slide5.PNG", false, null),
("5d029b03d48e0", 1, 6, "assets/imgs/slides/topic1/Slide6.PNG", false, null),
("5d029b1cb65bf", 1, 7, "assets/imgs/slides/topic1/Slide7.PNG", false, null),
("5d029b34d15a4", 1, 8, "assets/imgs/slides/topic1/Slide8.PNG", false, null),
("5d029b4c3dc01", 1, 9, "assets/imgs/slides/topic1/Slide9.PNG", false, null),
("5d029b790d178", 1, 10, "assets/imgs/slides/topic1/Slide10.gif", false, null),
("5d029ba2f2165", 1, 11, "assets/imgs/slides/topic1/Slide11.PNG", false, null),
("5d029bc08d793", 1, 12, "assets/imgs/slides/topic1/Slide12.PNG", false, null),
("5d029bf48ecaf", 1, 13, "assets/imgs/slides/topic1/Slide13.PNG", false, null),
("5d029bfef40c7", 1, 14, "assets/imgs/slides/topic1/Slide14.PNG", true, "print|John|print|2019|print|Blockly~John|2019|Blockly"),
("5d029c077c4fc", 1, 15, "assets/imgs/slides/topic1/Slide15.gif", false, null),
-- Variables
("5d7327ff6e869", 2, 1, "assets/imgs/slides/topic2/Slide16.png", false, null),
("5d7327ff6e86e", 2, 2, "assets/imgs/slides/topic2/Slide17.png", false, null),
("5d7327ff6e86f", 2, 3, "assets/imgs/slides/topic2/Slide18.png", false, null),
("5d7327ff6e870", 2, 4, "assets/imgs/slides/topic2/Slide19.png", false, null),
("5d7327ff6e871", 2, 5, "assets/imgs/slides/topic2/Slide20.png", false, null),
("5d7327ff6e872", 2, 6, "assets/imgs/slides/topic2/Slide21.png", false, null),
("5d7327ff6e873", 2, 7, "assets/imgs/slides/topic2/Slide22.png", false, null),
("5d7327ff6e874", 2, 8, "assets/imgs/slides/topic2/Slide23.gif", false, null), -- gif
("5d7327ff6e875", 2, 9, "assets/imgs/slides/topic2/Slide24.png", false, null),
("5d7327ff6e876", 2, 10, "assets/imgs/slides/topic2/Slide25.png", true, "=|Steve|=|1955|=|P~Steve|1955|P"), -- lec
("5d73287de2687", 2, 11, "assets/imgs/slides/topic2/Slide26.gif", false, null), -- gif
-- Arithmetic
("5d7345269c963", 3, 1, "assets/imgs/slides/topic3/Slide27.png", false, null),
("5d7345269c966", 3, 2, "assets/imgs/slides/topic3/Slide28.png", false, null),
("5d7345269c967", 3, 3, "assets/imgs/slides/topic3/Slide29.png", false, null),
("5d7345269c968", 3, 4, "assets/imgs/slides/topic3/Slide30.png", false, null),
("5d7345269c969", 3, 5, "assets/imgs/slides/topic3/Slide31.png", false, null),
("5d7345269c96a", 3, 6, "assets/imgs/slides/topic3/Slide32.png", false, null),
("5d7345269c96b", 3, 7, "assets/imgs/slides/topic3/Slide33.png", false, null),
("5d7345269c96c", 3, 8, "assets/imgs/slides/topic3/Slide34.gif", false, null), -- gif
("5d7345269c96d", 3, 9, "assets/imgs/slides/topic3/Slide35.png", true, "print|*~4050"), -- lec
("5d7345269c96e", 3, 10, "assets/imgs/slides/topic3/Slide36.gif", false, null), -- gif
("5d7345269c96f", 3, 11, "assets/imgs/slides/topic3/Slide37.png", false, null),
("5d7345269c970", 3, 12, "assets/imgs/slides/topic3/Slide38.png", true, "=|=|+|print~22"), -- lec
("5d7345269c971", 3, 13, "assets/imgs/slides/topic3/Slide39.gif", false, null), -- gif
("5d7345269c972", 3, 14, "assets/imgs/slides/topic3/Slide40.png", false, null),
("5d7345269c973", 3, 15, "assets/imgs/slides/topic3/Slide41.png", false, null),
("5d7345269c974", 3, 16, "assets/imgs/slides/topic3/Slide42.png", false, null),
("5d7345269c975", 3, 17, "assets/imgs/slides/topic3/Slide43.png", true, "print|*|-|**~24"), -- lec
("5d7345269c976", 3, 18, "assets/imgs/slides/topic3/Slide44.gif", false, null), -- gif
-- Relational
("5d734752ac6ce", 4, 1, "assets/imgs/slides/topic4/Slide45.png", false, null),
("5d734752ac6d1", 4, 2, "assets/imgs/slides/topic4/Slide46.png", false, null),
("5d734752ac6d2", 4, 3, "assets/imgs/slides/topic4/Slide47.png", false, null),
("5d734752ac6d3", 4, 4, "assets/imgs/slides/topic4/Slide48.png", false, null),
("5d734752ac6d4", 4, 5, "assets/imgs/slides/topic4/Slide49.png", false, null),
("5d734752ac6d5", 4, 6, "assets/imgs/slides/topic4/Slide50.gif", false, null), -- gif
("5d734752ac6d6", 4, 7, "assets/imgs/slides/topic4/Slide51.png", true, "print|/|==~true"), -- lec
("5d734752ac6d7", 4, 8, "assets/imgs/slides/topic4/Slide52.gif", false, null), -- gif
-- Conditional
("5d7347fce2a24", 5, 1, "assets/imgs/slides/topic5/Slide53.png", false, null),
("5d7347fce2a27", 5, 2, "assets/imgs/slides/topic5/Slide54.png", false, null),
("5d7347fce2a28", 5, 3, "assets/imgs/slides/topic5/Slide55.png", false, null),
("5d7347fce2a29", 5, 4, "assets/imgs/slides/topic5/Slide56.png", false, null),
("5d7347fce2a2a", 5, 5, "assets/imgs/slides/topic5/Slide57.png", false, null),
("5d7347fce2a2b", 5, 6, "assets/imgs/slides/topic5/Slide58.png", false, null),
("5d7347fce2a2c", 5, 7, "assets/imgs/slides/topic5/Slide59.gif", false, null), -- gif
("5d7347fce2a2d", 5, 8, "assets/imgs/slides/topic5/Slide60.png", false, null),
("5d7347fce2a2e", 5, 9, "assets/imgs/slides/topic5/Slide61.png", false, null),
("5d7347fce2a2f", 5, 10, "assets/imgs/slides/topic5/Slide62.png", false, null),
("5d7347fce2a30", 5, 11, "assets/imgs/slides/topic5/Slide63.png", false, null),
("5d7347fce2a31", 5, 12, "assets/imgs/slides/topic5/Slide64.gif", false, null), -- gif
("5d7347fce2a32", 5, 13, "assets/imgs/slides/topic5/Slide65.png", true, "if|&|gt|;|print|else|print~25"), -- lec
("5d7347fce2a33", 5, 14, "assets/imgs/slides/topic5/Slide66.gif", false, null), -- gif
-- Logical
("5d7348f6d7afa", 6, 1, "assets/imgs/slides/topic6/Slide67.png", false, null),
("5d7348f6d7afd", 6, 2, "assets/imgs/slides/topic6/Slide68.png", false, null),
("5d7348f6d7afe", 6, 3, "assets/imgs/slides/topic6/Slide69.png", false, null),
("5d7348f6d7aff", 6, 4, "assets/imgs/slides/topic6/Slide70.png", false, null),
("5d7348f6d7b00", 6, 5, "assets/imgs/slides/topic6/Slide71.gif", false, null), -- gif
("5d7348f6d7b01", 6, 6, "assets/imgs/slides/topic6/Slide72.png", false, null),
("5d7348f6d7b02", 6, 7, "assets/imgs/slides/topic6/Slide73.png", false, null),
("5d7348f6d7b03", 6, 8, "assets/imgs/slides/topic6/Slide74.png", false, null),
("5d7348f6d7b04", 6, 9, "assets/imgs/slides/topic6/Slide75.png", true, "if|&|lt|;|and|!=|print|else|print~Both|conditions|correct"), -- lec
("5d7348f6d7b05", 6, 10, "assets/imgs/slides/topic6/Slide76.gif", false, null), -- gif
("5d7348f6d7b06", 6, 11, "assets/imgs/slides/topic6/Slide77.png", false, null),
("5d7348f6d7b07", 6, 12, "assets/imgs/slides/topic6/Slide78.png", false, null),
("5d7348f6d7b08", 6, 13, "assets/imgs/slides/topic6/Slide79.png", false, null),
("5d7348f6d7b09", 6, 14, "assets/imgs/slides/topic6/Slide80.png", true, "if|==|or|** 2|==|print|else|print~One|condition|correct"), -- lec
("5d7348f6d7b0a", 6, 15, "assets/imgs/slides/topic6/Slide81.gif", false, null), -- gif
("5d7348f6d7b0b", 6, 16, "assets/imgs/slides/topic6/Slide82.png", false, null),
("5d7348f6d7b0c", 6, 17, "assets/imgs/slides/topic6/Slide83.png", false, null),
("5d7348f6d7b0d", 6, 18, "assets/imgs/slides/topic6/Slide84.png", false, null),
("5d7348f6d7b0e", 6, 19, "assets/imgs/slides/topic6/Slide85.png", true, "if|not|print|else|print~Finished"), -- lec
("5d7348f6d7b0f", 6, 20, "assets/imgs/slides/topic6/Slide86.gif", false, null), -- gif
-- Iterative
("5d7349aeae0f0", 7, 1, "assets/imgs/slides/topic7/Slide87.png", false, null),
("5d7349aeae0f3", 7, 2, "assets/imgs/slides/topic7/Slide88.png", false, null),
("5d7349aeae0f4", 7, 3, "assets/imgs/slides/topic7/Slide89.png", false, null),
("5d7349aeae0f5", 7, 4, "assets/imgs/slides/topic7/Slide90.png", false, null),
("5d7349aeae0f6", 7, 5, "assets/imgs/slides/topic7/Slide91.png", false, null),
("5d7349aeae0f7", 7, 6, "assets/imgs/slides/topic7/Slide92.gif", false, null), -- gif
("5d7349aeae0f8", 7, 7, "assets/imgs/slides/topic7/Slide93.png", false, null),
("5d7349aeae0f9", 7, 8, "assets/imgs/slides/topic7/Slide94.png", false, null),
("5d7349aeae0fa", 7, 9, "assets/imgs/slides/topic7/Slide95.png", true, "for|in range|=|+|print~12"), -- lec
("5d7349aeae0fb", 7, 10, "assets/imgs/slides/topic7/Slide96.gif", false, null); -- gif

INSERT INTO QUESTIONS VALUES
-- Basics
("5d041d0166b4e", 1, "Programming Languages are tools used to _____________.",
  "translate natural language to machine language","create computer programs", "modify computer settings", "B"),
("5d041d0166b4f", 1, "What is computer programming?",
  "Speeding up your computer.","Building a computer.", "Telling the computer what to do through a special set of instructions.", "C"),
("5d041d0166b50", 1, "Like the English language, a Programming Language also has words, symbols and grammatical rules.",
  "True", "False", null, "A"),
("5d041d0166b51", 1, "_____________ refers to the correct ‘spelling’ and ‘grammar’ of a Programming Language.",
  "Syntax", "Source Code", "Machine Language", "A"),
("5d041d0166b52", 1, "_____________is a person who writes computer programs.",
  "Programmer", "Coder", "Hacker", "A"),
("5d041d0166b53", 1, "Humans are capable of expressing their thoughts in 1’s and 0’s.",
  "True", "False", null, "B"),
("5d041d0166b54", 1, "How does programming work?",
  "Machine Code -> Compiling -> Source Code", "Compiling -> Source Code -> Machine Code", "Source Code -> Compiling -> Machine Code", "C"),
("5d041d0166b55", 1, "_____________ are the list of predefined words or reserved words used in programming languages that cannot be used as identifiers.",
  "Keywords", "Operators", "None of the above.", "A"),
("5d2dd6917581e", 1, "Display “Hello World” using print. Give the proper python syntax.",
  null, null, null, "print|Hello|World"),
("5d041d0166b56", 1, "Given the code snippet below, what will be the output of the program?<p class='snippet'>print(‘Hello')<br>print('world’)</p>",
  "<p class='snippet'>Hello world</p>", "<p class='snippet'>HelloWord</p>", "<p class='snippet'>Hello<br>world</p>", "C"),
-- Variables
("5d041d0166b58", 1, "Identifiers cannot have special characters in them.",
  "True", "False", null, "A"),
("5d041d0166b59", 1, "Keywords can be used as variable identifiers.",
  "True", "False", null, "B"),
("5d041d0166b5a", 1, "In Python, which of the following signs is used to assign values to variables?",
  "==", "->", "=", "C"),
("5d76c5623fdb4", 1, "How do you assign a value to a variable? Give the proper python syntax.",
  null, null, null, "variable|=|value"),
("5d041d0166b5b", 1, "Suppose you have this variable assignment, what is the value of the y variable?<p class='snippet'>x = 50</p><p class='snippet'>y = 15</p>",
  "15", "50", "5", "A"),
("5d041d0166b5c", 1, "Which of the following is a valid identifier?",
  "print", "_22", "1one", "B"),
("5d041d0166b48", 1, "Accessing a variable is not case-sensitive. Suppose you have a variable called 'month', it can be accessed by using month, Month, or MONTH.",
  "True", "False", null, "B"),
("5d76c5623fdba", 1, "Create a variable called 'name'. Assign the text 'Guido' to the variable created. Then, display the value of the variable",
  null, null, null, "name|=|Guido|print|name"),
-- Arithmetic
("5d7749871a1be", 2, "Which of the following is NOT a category of operators in python?",
  "Logical", "Rational", "Arithmetic", "B"),
("5d7749871a1c1", 2, "_____________ is a mathematical function that computes two operands.",
  "Relational Operators", "Assignment Operators", "Arithmetic Operators", "C"),
("5d7749871a1c2", 2, "In python, what symbol should you use if you want to perform division?",
  "÷", "/", "&", "B"),
("5d7749871a1c3", 2, "What is the equivalent of 7 raised to 2 in python?",
  "7 ** 2", "7 ^ 2", "7 * 2", "A"),
("5d7749871a1c4", 2, "In Python, what is the correct arithmetic precedence?<table class='table table-bordered text-center'><tbody><tr><td>A - Addition</td><td>E - Exponential</td></tr><tr><td>D - Division</td><td>M - Multiplication</td></tr><tr><td>P - Parentheses</td><td>S - Subtraction</td></tr></tbody></table>",
  "PESMAD", "PEMSAD", "PEMDAS", "C"),
("5d7749871a1c5", 2, "Given this code snippet,<p class='snippet'>test = 2 * 3 - 1 + 2</p>What will be the value of test?",
  "7", "6", "3", "A"),
("5d7749871a1c6", 2, "Variables can be used to store the result of a mathematical operation.",
  "True", "False", null, "A"),
("5d7749871a1c7", 2, "Given the following code, is the value of num1 the same with num2?<p class='snippet'>num1 = (20 - 5 / 3)<br>num2 = ((20 - 5) / 3)</p>",
  "True", "False", null, "A"),
("5d7749871a1c8", 2, "Which of the following has the correct python syntax for variable assignment?",
  "1 + 1 = sum", "sum = 1 + 1", "sum = 1 + 1 = sum", "B"),
("5d7749871a1c9", 2, "Create a Python program that multiplies 2 and 10. Store the result of a variable named 'product'.",
  null, null, null, "product|=|*"),
-- Relational
("5d7749871a1ca", 2, "These symbols are used to compare two values.",
  "Relational Operators", "Assignment Operators", "Arithmetic Operators", "A"),
("5d7749871a1cc", 2, "In python what symbol is used if the variable on the left is less than or equal to the right variable?",
  "==", ">=", "<=", "C"),
-- Conditional
("5d7749871a1cf", 2, "Given the following code snippet, what will be the output of the program?<p class='snippet'>num = 5<br>if num == 5:<br>&nbsp;&nbsp;print('The number is 5')</p>",
  "5", "'The number is 5'", "The number is 5", "C"),
("5d7749871a1d0", 2, "Which of the following is the correct Python syntax for an if-else statement?",
  "<p class='snippet'>if condition:<br>&nbsp;&nbsp;statements/expressions<br>else:<br>&nbsp;&nbsp;statements/expressions</p>", "<p class='snippet'>if statement then:<br>&nbsp;&nbsp;statements/expressions<br>else:<br>&nbsp;&nbsp;statements/expressions</p>", "None of the above", "A"),
("5d7749871a1d1", 2, "Given the following code snippet, what will be the output of the program?<p class='snippet'>age = 18<br>if age < 12:<br>&nbsp;&nbsp;print('Child')<br>else:<br>&nbsp;&nbsp;print('Adult')</p>",
  "Child", "Adult", "Error", "B"),
-- Logical
("5d775314f1204", 3, "OR, AND and NOT are __________ operators?",
  "Arithmetic", "Logical", "Relational", "B"),
("5d775314f1207", 3, "The AND operator returns False when atleast one of the operands is False.",
  "True", "False", null, "A"),
("5d775314f1208", 3, "Given this logical expression<p class='snippet'>(1 = 1) OR (5 >= 8)</p>what will be the value of the expression?",
  "True", "False", "Error", "A"),
("5d775314f1209", 3, "This logical operator returns True if atleast one of the operands is true.",
  "NOT", "OR", "AND", "B"),
("5d775314f120a", 3, "Suppose you have this logical expression<p class='snippet'>(1 > 0) AND (0 < 1)</p>what will be the value of the expression?",
  "True", "False", "Error", "A"),
("5d775314f120b", 3, "Given the following code snippet, what will be the value of 'isMarried'?<p class='snippet'>isMarried = true<br>isMarried = not isMarried</p>",
  "True", "False", null, "B"),
("5d775314f120c", 3, "Suppose you have this logical expression, <p class='snippet'>4 <= 4</p>what will be the value of the expression?",
  "True", "False", null, "A"),
-- Iterative
("5d775314f120d", 3, "_____________ is a sequence of instructions that is continuously repeated until a certain condition is satisfied.",
  "Iterative Statements", "Loops", "All of the above", "C"),
("5d775314f120e", 3, "For Loop is an example of an iterative statement.",
  "True", "False", null, "A"),
("5d775314f120f", 3, "In the execution of a For-Loop, how many parts does it have?",
  "2", "3", "4", "B"),
("5d775314f1210", 3, "Given the following code snippet, what does n represents?<p class='snippet'>for count in range(n):</p>",
  "Number of random values", "Number of output", "Number of iterations", "C"),
("5d775314f1211", 3, "Create a program that will output “I love CodeInBlocks” 10 times.",
  null, null, null, "for|in|range|10|print|I love CodeInBlocks");

INSERT INTO ACTIVITIES VALUES
("cib00000", "Welcome to CodeinBlocks!", "We've noticed that you're new here. We recommend clicking the button below your username on the top-right of the page or the button below for a tour of the system.<br><a id='systour'></a>"),
("cib00001", "You've cleared the 1st Topic!", "Here's a general fact: 'A good programmer is someone who always looks both ways before crossing a one-way street.' The question is, why?..."),
("cib00002", "You've cleared the 2nd Topic!", "Programmer (noun)<br>- a person who fixed a problem that you don’t know you have, in a way you don’t understand."),
("cib00003", "You've cleared the 3rd Topic!", "Trivia for you: Did you know how many total programming languages? – it’s 698.<br>Well, it doesn't really matter. All of them gets compiled to machine language anyway."),
("cib00004", "You've cleared the 4th Topic!", "Congratulations! Most people are intimidated by the thought of learning how to program, however, the more you practice and repeatedly do that task, the easier it gets."),
("cib00005", "You've cleared the 5th Topic!", "Hey, let me give you a tip:<br>  Ctrl + C and Ctrl + V have saved more lives than Batman and Robin. *wink wink*"),
("cib00006", "You've cleared the 6th Topic!", "Programming is 10% writing code and 90% understanding why it’s not working. Wouldn't you agree?"),
("cib00007", "You've cleared the 7th Topic!", "Did you know? Real programmers count from 0. Though its not very relevant in Blockly, it is with other programming languages."),

("cibw0001", "1st worksheet Done!", "Great job! You can go back and try any worksheet again. Only your most recent score is recorded."),
("cibw0002", "2nd worksheet Done!", "You can use the Sandbox to practice or try any idea you may have for Blockly."),
("cibw0003", "Did you figure out the my bug?", "<b>Congratulations! You've finished all the sessions.<b>There a bug in the system. Did you find it? Let us know.");


INSERT INTO ACTIVITY_LOGS VALUES
("2019-09-06 01:47:14", "5d01dabf6e58c", "cib00000");


DROP PROCEDURE IF EXISTS stuckParticipant;
DELIMITER //
CREATE PROCEDURE stuckParticipant(IN name varchar(30), IN topic int)
BEGIN
  update PROGRESS
  set curr_slide = 1
  where user_ID = (select user_ID from USER_PROFILES where username = name)
  and topic_num = topic+1;
END; //
DELIMITER ;

COMMIT;
