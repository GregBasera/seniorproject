-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: codeinblocks
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ACTIVITIES`
--

DROP TABLE IF EXISTS `ACTIVITIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACTIVITIES` (
  `activity_ID` varchar(8) NOT NULL,
  `title` varchar(60) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`activity_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACTIVITIES`
--

LOCK TABLES `ACTIVITIES` WRITE;
/*!40000 ALTER TABLE `ACTIVITIES` DISABLE KEYS */;
INSERT INTO `ACTIVITIES` VALUES
('cib00000','Welcome to CodeinBlocks!','We\'ve noticed that you\'re new here. We recommend clicking the button below your username on the top-right of the page or the button below for a tour of the system.<br><a id=\'systour\'></a>'),
('cib00001','You\'ve cleared the 1st Topic!','Here\'s a general fact: \'A good programmer is someone who always looks both ways before crossing a one-way street.\' The question is, why?...'),
('cib00002','You\'ve cleared the 2nd Topic!','Programmer (noun)<br>- a person who fixed a problem that you don’t know you have, in a way you don’t understand.'),
('cib00003','You\'ve cleared the 3rd Topic!','Trivia for you: Did you know how many total programming languages? – it’s 698.<br>Well, it doesn\'t really matter. All of them gets compiled to machine language anyway.'),
('cib00004','You\'ve cleared the 4th Topic!','Congratulations! Most people are intimidated by the thought of learning how to program, however, the more you practice and repeatedly do that task, the easier it gets.'),
('cib00005','You\'ve cleared the 5th Topic!','Hey, let me give you a tip:<br>  Ctrl + C and Ctrl + V have saved more lives than Batman and Robin. *wink wink*'),
('cib00006','You\'ve cleared the 6th Topic!','Programming is 10% writing code and 90% understanding why it’s not working. Wouldn\'t you agree?'),
('cib00007','You\'ve cleared the 7th Topic!','Did you know? Real programmers count from 0. Though its not very relevant in Blockly, it is with other programming languages.'),
('cibw0001','1st worksheet Done!','Great job! You can go back and try any worksheet again. Only your most recent score is recorded.'),
('cibw0002','2nd worksheet Done!','You can use the Sandbox to practice or try any idea you may have for Blockly.'),
('cibw0003','Did you figure out the my bug?','<b>Congratulations! You\'ve finished all the sessions.<b>There a bug in the system. Did you find it? Let us know.');
/*!40000 ALTER TABLE `ACTIVITIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACTIVITY_LOGS`
--

DROP TABLE IF EXISTS `ACTIVITY_LOGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACTIVITY_LOGS` (
  `date_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_ID` varchar(15) NOT NULL,
  `activity_ID` varchar(8) NOT NULL,
  KEY `user_ID` (`user_ID`),
  KEY `activity_ID` (`activity_ID`),
  CONSTRAINT `ACTIVITY_LOGS_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `USER_PROFILES` (`user_ID`),
  CONSTRAINT `ACTIVITY_LOGS_ibfk_2` FOREIGN KEY (`activity_ID`) REFERENCES `ACTIVITIES` (`activity_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACTIVITY_LOGS`
--

LOCK TABLES `ACTIVITY_LOGS` WRITE;
/*!40000 ALTER TABLE `ACTIVITY_LOGS` DISABLE KEYS */;
INSERT INTO `ACTIVITY_LOGS` VALUES
('2019-09-06 01:47:14','5d01dabf6e58c','cib00000'),
('2019-10-18 17:47:43','5da9fabee46fa','cib00000'),
('2019-10-18 17:48:33','5d01dabf6e58c','cib00001'),
('2019-10-18 17:53:47','5d01dabf6e58c','cib00002'),
('2019-10-18 17:54:55','5d01dabf6e58c','cibw0001'),
('2019-10-18 18:00:58','5da9fdda1b5e0','cib00000'),
('2019-10-18 18:03:57','5d01dabf6e58c','cib00003'),
('2019-10-18 18:04:20','5da9fdda1b5e0','cib00001'),
('2019-10-18 18:04:52','5d01dabf6e58c','cib00004'),
('2019-10-18 18:06:42','5d01dabf6e58c','cib00005'),
('2019-10-18 18:07:08','5d01dabf6e58c','cibw0002'),
('2019-10-18 18:13:23','5d01dabf6e58c','cib00006'),
('2019-10-18 18:14:40','5d01dabf6e58c','cib00007'),
('2019-10-18 18:14:59','5d01dabf6e58c','cibw0003'),
('2019-10-19 00:44:13','5daa5c5d0e58b','cib00000'),
('2019-10-19 00:44:52','5daa5c8461a26','cib00000'),
('2019-10-19 00:45:01','5daa5c8da4475','cib00000'),
('2019-10-19 00:45:11','5daa5c972a89e','cib00000'),
('2019-10-19 00:57:10','5d01dabf6e58c','cib00001'),
('2019-10-19 00:58:51','5daa5c5d0e58b','cib00001'),
('2019-10-19 01:00:32','5daa5c8da4475','cib00001'),
('2019-10-19 01:01:15','5daa5c8461a26','cib00001'),
('2019-10-19 01:01:42','5daa5c972a89e','cib00001'),
('2019-10-19 01:15:57','5daa5c8da4475','cib00002'),
('2019-10-19 01:16:52','5daa5c8461a26','cib00002'),
('2019-10-19 01:21:23','5daa5c8461a26','cibw0001'),
('2019-10-19 01:22:42','5daa5c8da4475','cibw0001'),
('2019-10-19 01:34:32','5d01dabf6e58c','cib00002'),
('2019-10-19 01:35:16','5d01dabf6e58c','cib00003'),
('2019-10-19 01:36:14','5d01dabf6e58c','cib00003'),
('2019-10-19 01:40:40','5daa5c8461a26','cib00003'),
('2019-10-19 01:40:52','5daa5c5d0e58b','cib00002'),
('2019-10-19 01:41:06','5d01dabf6e58c','cib00003'),
('2019-10-19 01:41:26','5daa5c8da4475','cib00003'),
('2019-10-19 01:45:25','5daa5c8da4475','cib00004'),
('2019-10-19 01:46:21','5daa5c8461a26','cib00004'),
('2019-10-19 01:54:05','5daa5c5d0e58b','cibw0001'),
('2019-10-19 02:05:11','5daa5c8461a26','cib00005'),
('2019-10-19 02:08:39','5daa5c8da4475','cib00005'),
('2019-10-19 02:09:56','5daa5c8461a26','cibw0002'),
('2019-10-19 02:10:14','5daa5c972a89e','cib00002'),
('2019-10-19 02:13:35','5daa5c8da4475','cibw0002'),
('2019-10-19 02:21:03','5daa730edeedf','cib00000'),
('2019-10-19 02:21:03','5daa5c972a89e','cibw0001'),
('2019-10-19 02:21:24','5daa5c5d0e58b','cib00003'),
('2019-10-19 02:21:39','5daa73334e4d8','cib00000'),
('2019-10-19 02:26:29','5daa5c8461a26','cib00006'),
('2019-10-19 02:28:35','5daa74d3ab3d9','cib00000'),
('2019-10-19 02:28:38','5daa74d6ceb56','cib00000'),
('2019-10-19 02:31:25','5daa5c8da4475','cib00006'),
('2019-10-19 02:34:57','5daa76517e866','cib00000'),
('2019-10-19 02:37:21','5daa73334e4d8','cib00001'),
('2019-10-19 02:37:28','5daa76e823818','cib00000'),
('2019-10-19 02:38:11','5daa7713be2c2','cib00000'),
('2019-10-19 02:43:43','5daa5c5d0e58b','cib00004'),
('2019-10-19 02:44:27','5daa5c8da4475','cib00007'),
('2019-10-19 02:44:43','5daa5c8461a26','cib00007'),
('2019-10-19 02:46:05','5daa74d3ab3d9','cib00001'),
('2019-10-19 02:46:17','5daa5c8461a26','cibw0003'),
('2019-10-19 02:46:28','5daa7713be2c2','cib00001'),
('2019-10-19 02:47:18','5daa76e823818','cib00001'),
('2019-10-19 02:47:38','5daa5c972a89e','cib00003'),
('2019-10-19 02:47:42','5daa5c8461a26','cibw0003'),
('2019-10-19 02:49:59','5daa5c8da4475','cibw0003'),
('2019-10-19 02:52:35','5daa73334e4d8','cib00002'),
('2019-10-19 02:53:00','5daa5c972a89e','cib00004'),
('2019-10-19 02:55:09','5daa76517e866','cib00001'),
('2019-10-19 02:55:14','5daa74d6ceb56','cib00001'),
('2019-10-19 02:58:21','5daa730edeedf','cib00001'),
('2019-10-19 03:07:08','5daa73334e4d8','cibw0001'),
('2019-10-19 03:07:38','5daa74d3ab3d9','cib00002'),
('2019-10-19 03:07:48','5daa7713be2c2','cib00002'),
('2019-10-19 03:10:53','5daa76e823818','cib00002'),
('2019-10-19 03:15:31','5daa7fd399474','cib00000'),
('2019-10-19 03:19:56','5daa76e823818','cibw0001'),
('2019-10-19 03:20:57','5daa7713be2c2','cibw0001'),
('2019-10-19 03:22:18','5daa76517e866','cib00002'),
('2019-10-19 03:22:20','5daa74d6ceb56','cib00002'),
('2019-10-19 03:22:37','5daa73334e4d8','cib00003'),
('2019-10-19 03:22:52','5daa7fd399474','cib00001'),
('2019-10-19 03:23:23','5daa74d3ab3d9','cibw0001'),
('2019-10-19 03:28:31','5daa73334e4d8','cib00004'),
('2019-10-19 03:29:50','5daa730edeedf','cib00002'),
('2019-10-19 03:30:04','5daa74d6ceb56','cibw0001'),
('2019-10-19 03:33:48','5daa76517e866','cibw0001'),
('2019-10-19 03:35:31','5daa730edeedf','cibw0001'),
('2019-10-19 03:39:52','5daa76e823818','cib00003'),
('2019-10-19 03:39:53','5daa7713be2c2','cib00003'),
('2019-10-19 03:40:47','5daa74d3ab3d9','cib00003'),
('2019-10-19 03:41:25','5daa7fd399474','cib00002'),
('2019-10-19 03:43:10','5daa74d6ceb56','cib00003'),
('2019-10-19 03:43:26','5daa73334e4d8','cib00005'),
('2019-10-19 03:45:59','5daa74d3ab3d9','cib00004'),
('2019-10-19 03:46:07','5daa74d6ceb56','cib00004'),
('2019-10-19 03:46:11','5daa74d3ab3d9','cib00004'),
('2019-10-19 03:47:57','5daa7713be2c2','cib00004'),
('2019-10-19 03:48:05','5daa76e823818','cib00004'),
('2019-10-19 03:49:11','5daa73334e4d8','cibw0002'),
('2019-10-19 03:50:21','5daa7fd399474','cibw0001'),
('2019-10-19 03:58:57','5daa74d3ab3d9','cib00005'),
('2019-10-19 04:00:01','5d01dabf6e58c','cib00002'),
('2019-10-19 04:00:04','5daa76e823818','cib00005'),
('2019-10-19 04:00:58','5daa76517e866','cib00003'),
('2019-10-19 04:01:36','5daa74d6ceb56','cib00005'),
('2019-10-19 04:01:50','5daa76e823818','cibw0002'),
('2019-10-19 04:02:08','5daa7713be2c2','cib00005'),
('2019-10-19 04:04:00','5daa730edeedf','cib00003'),
('2019-10-19 04:04:40','5daa74d3ab3d9','cibw0002'),
('2019-10-19 04:04:59','5daa7713be2c2','cibw0002'),
('2019-10-19 04:06:54','5daa74d6ceb56','cibw0002'),
('2019-10-19 04:10:09','5daa76517e866','cib00004'),
('2019-10-19 04:10:34','5daa730edeedf','cib00004'),
('2019-10-19 04:16:27','5daa73334e4d8','cib00006'),
('2019-10-19 04:17:35','5daa73334e4d8','cib00006'),
('2019-10-19 04:23:24','5daa7fd399474','cib00003'),
('2019-10-19 04:27:00','5daa74d3ab3d9','cib00006'),
('2019-10-19 04:27:13','5daa730edeedf','cib00005'),
('2019-10-19 04:27:32','5daa7fd399474','cib00004'),
('2019-10-19 04:28:05','5daa73334e4d8','cib00007'),
('2019-10-19 04:29:34','5daa74d3ab3d9','cib00006'),
('2019-10-19 04:30:57','5daa7713be2c2','cib00006'),
('2019-10-19 04:32:07','5daa76517e866','cib00005'),
('2019-10-19 04:33:04','5daa73334e4d8','cibw0003'),
('2019-10-19 04:33:11','5daa730edeedf','cibw0002'),
('2019-10-19 04:33:44','5daa74d6ceb56','cib00006'),
('2019-10-19 04:36:51','5daa7fd399474','cib00005'),
('2019-10-19 04:38:03','5daa76517e866','cibw0002'),
('2019-10-19 04:44:48','5daa74d3ab3d9','cib00007'),
('2019-10-19 04:45:53','5daa7713be2c2','cib00007'),
('2019-10-19 04:46:04','5daa76e823818','cib00006'),
('2019-10-19 04:46:16','5daa7fd399474','cibw0002'),
('2019-10-19 04:46:56','5daa74d6ceb56','cib00007'),
('2019-10-19 04:47:36','5daa76e823818','cib00007'),
('2019-10-19 04:52:22','5daa7713be2c2','cibw0003'),
('2019-10-19 04:52:25','5daa76e823818','cibw0003'),
('2019-10-19 04:52:39','5daa74d6ceb56','cibw0003'),
('2019-10-19 04:52:53','5daa74d3ab3d9','cibw0003'),
('2019-10-19 05:06:58','5daa730edeedf','cib00006'),
('2019-10-19 05:10:08','5daa76517e866','cib00006'),
('2019-10-19 05:17:36','5daa76517e866','cib00007'),
('2019-10-19 05:18:29','5daa7fd399474','cib00006'),
('2019-10-19 05:22:01','5daa76517e866','cibw0003'),
('2019-10-19 05:23:02','5daa730edeedf','cib00007'),
('2019-10-19 05:25:15','5daa7fd399474','cib00007'),
('2019-10-19 05:27:53','5daa730edeedf','cibw0003'),
('2019-10-19 05:32:49','5daa7fd399474','cibw0003'),
('2019-10-24 03:43:13','5db11dd0ee82f','cib00000'),
('2019-10-24 03:49:25','5db11dd0ee82f','cib00001'),
('2019-10-24 03:55:34','5db11dd0ee82f','cib00002'),
('2019-10-24 03:59:04','5db11dd0ee82f','cibw0001'),
('2019-10-24 04:16:52','5db11dd0ee82f','cib00003'),
('2019-10-24 04:23:17','5db11dd0ee82f','cib00004'),
('2019-10-24 04:32:15','5db11dd0ee82f','cib00005'),
('2019-10-24 04:36:13','5db11dd0ee82f','cibw0002'),
('2019-10-24 04:59:36','5db11dd0ee82f','cib00006'),
('2019-10-24 05:09:37','5db11dd0ee82f','cib00007'),
('2019-10-24 05:12:49','5db11dd0ee82f','cibw0003'),
('2019-10-24 20:58:32','5d01dabf6e58c','cib00004'),
('2019-10-26 03:35:05','5db3bee967e2c','cib00000'),
('2019-10-26 03:35:31','5db3bf039e37c','cib00000'),
('2019-10-26 03:35:33','5db3bf050a8a0','cib00000'),
('2019-10-26 03:36:40','5db3bf48238aa','cib00000'),
('2019-10-26 03:37:50','5db3bf8e02420','cib00000'),
('2019-10-26 03:39:05','5db3bfd923aac','cib00000'),
('2019-10-26 03:40:46','5db3c03e7e028','cib00000'),
('2019-10-26 03:42:32','5db3bf48238aa','cib00001'),
('2019-10-26 03:45:23','5db3c153c2bdb','cib00000'),
('2019-10-26 03:46:12','5db3bf039e37c','cib00001'),
('2019-10-26 03:46:37','5db3bf050a8a0','cib00001'),
('2019-10-26 03:47:08','5db3bf8e02420','cib00001'),
('2019-10-26 03:47:37','5db3bfd923aac','cib00001'),
('2019-10-26 03:49:23','5db3c03e7e028','cib00001'),
('2019-10-26 03:50:21','5db3c153c2bdb','cib00001'),
('2019-10-26 03:50:42','5db3c291da349','cib00000'),
('2019-10-26 03:51:05','5db3bf48238aa','cib00002'),
('2019-10-26 03:51:58','5db3bf039e37c','cib00002'),
('2019-10-26 03:51:59','5db3bee967e2c','cib00001'),
('2019-10-26 03:52:38','5db3bf050a8a0','cib00002'),
('2019-10-26 03:53:03','5db3c31f132a2','cib00000'),
('2019-10-26 03:53:53','5db3bf48238aa','cibw0001'),
('2019-10-26 03:54:41','5db3c03e7e028','cib00002'),
('2019-10-26 03:57:05','5db3c31f132a2','cib00001'),
('2019-10-26 03:57:10','5db3bf050a8a0','cibw0001'),
('2019-10-26 03:57:18','5db3c291da349','cib00001'),
('2019-10-26 03:57:51','5db3c153c2bdb','cib00002'),
('2019-10-26 03:57:51','5db3bf039e37c','cibw0001'),
('2019-10-26 03:59:15','5db3bee967e2c','cib00002'),
('2019-10-26 04:00:26','5db3c4da5de19','cib00000'),
('2019-10-26 04:01:22','5db3c31f132a2','cib00002'),
('2019-10-26 04:01:52','5db3c153c2bdb','cibw0001'),
('2019-10-26 04:02:25','5db3c291da349','cib00002'),
('2019-10-26 04:02:28','5db3bf050a8a0','cib00003'),
('2019-10-26 04:02:39','5db3c03e7e028','cibw0001'),
('2019-10-26 04:02:47','5db3c567451b2','cib00000'),
('2019-10-26 04:03:07','5db3c4da5de19','cib00001'),
('2019-10-26 04:03:12','5db3bee967e2c','cibw0001'),
('2019-10-26 04:04:16','5db3c31f132a2','cibw0001'),
('2019-10-26 04:04:47','5db3bf050a8a0','cib00004'),
('2019-10-26 04:05:45','5db3bf039e37c','cib00003'),
('2019-10-26 04:06:04','5db3c567451b2','cib00001'),
('2019-10-26 04:06:07','5db3c291da349','cibw0001'),
('2019-10-26 04:06:59','5db3bf8e02420','cib00002'),
('2019-10-26 04:07:19','5db3bfd923aac','cib00002'),
('2019-10-26 04:08:36','5db3c6c4465d2','cib00000'),
('2019-10-26 04:08:45','5db3bf050a8a0','cib00005'),
('2019-10-26 04:08:48','5db3c6d01f5da','cib00000'),
('2019-10-26 04:10:21','5db3c72d39108','cib00000'),
('2019-10-26 04:10:25','5db3c31f132a2','cib00003'),
('2019-10-26 04:10:27','5db3bfd923aac','cibw0001'),
('2019-10-26 04:11:29','5db3c31f132a2','cib00004'),
('2019-10-26 04:11:36','5db3bf050a8a0','cibw0002'),
('2019-10-26 04:12:06','5db3bee967e2c','cib00003'),
('2019-10-26 04:12:25','5db3bf039e37c','cib00004'),
('2019-10-26 04:12:39','5db3bf8e02420','cibw0001'),
('2019-10-26 04:14:37','5db3bee967e2c','cib00004'),
('2019-10-26 04:14:51','5db3bf48238aa','cib00003'),
('2019-10-26 04:14:54','5db3bf039e37c','cib00005'),
('2019-10-26 04:15:37','5db3c03e7e028','cib00003'),
('2019-10-26 04:16:03','5db3c153c2bdb','cib00003'),
('2019-10-26 04:16:12','5db3c72d39108','cib00001'),
('2019-10-26 04:16:52','5db3c6d01f5da','cib00001'),
('2019-10-26 04:16:54','5db3c291da349','cib00003'),
('2019-10-26 04:17:10','5db3c6c4465d2','cib00001'),
('2019-10-26 04:17:31','5db3c31f132a2','cib00005'),
('2019-10-26 04:18:16','5db3c567451b2','cib00002'),
('2019-10-26 04:18:32','5db3bf039e37c','cibw0002'),
('2019-10-26 04:18:34','5db3c03e7e028','cib00004'),
('2019-10-26 04:18:46','5db3bfd923aac','cib00003'),
('2019-10-26 04:18:57','5db3bee967e2c','cib00005'),
('2019-10-26 04:19:49','5db3c31f132a2','cibw0002'),
('2019-10-26 04:22:04','5db3bfd923aac','cib00004'),
('2019-10-26 04:22:09','5db3c03e7e028','cib00005'),
('2019-10-26 04:22:17','5db3bee967e2c','cibw0002'),
('2019-10-26 04:22:22','5db3c6d01f5da','cib00002'),
('2019-10-26 04:22:46','5db3c291da349','cib00004'),
('2019-10-26 04:23:25','5db3bf050a8a0','cib00006'),
('2019-10-26 04:23:47','5db3c72d39108','cib00002'),
('2019-10-26 04:25:04','5db3c4da5de19','cib00002'),
('2019-10-26 04:25:22','5db3c03e7e028','cibw0002'),
('2019-10-26 04:25:28','5db3c567451b2','cibw0001'),
('2019-10-26 04:26:18','5db3bf050a8a0','cib00007'),
('2019-10-26 04:26:40','5db3c291da349','cib00005'),
('2019-10-26 04:27:39','5db3c6d01f5da','cibw0001'),
('2019-10-26 04:28:53','5db3bfd923aac','cib00005'),
('2019-10-26 04:29:11','5db3bf050a8a0','cibw0003'),
('2019-10-26 04:29:47','5db3c4da5de19','cibw0001'),
('2019-10-26 04:30:11','5db3c291da349','cibw0002'),
('2019-10-26 04:30:30','5db3c72d39108','cibw0001'),
('2019-10-26 04:30:48','5db3c31f132a2','cib00006'),
('2019-10-26 04:31:12','5db3bf050a8a0','cibw0002'),
('2019-10-26 04:31:27','5db3c153c2bdb','cib00004'),
('2019-10-26 04:32:12','5db3bf039e37c','cib00006'),
('2019-10-26 04:32:23','5db3bf050a8a0','cibw0002'),
('2019-10-26 04:32:40','5db3bf8e02420','cib00003'),
('2019-10-26 04:32:53','5db3bfd923aac','cibw0002'),
('2019-10-26 04:32:53','5db3c6c4465d2','cib00002'),
('2019-10-26 04:33:11','5db3bf039e37c','cib00006'),
('2019-10-26 04:33:50','5db3c153c2bdb','cib00005'),
('2019-10-26 04:33:59','5db3bf050a8a0','cibw0002'),
('2019-10-26 04:34:31','5db3c31f132a2','cib00007'),
('2019-10-26 04:35:07','5db3bf48238aa','cib00004'),
('2019-10-26 04:35:07','5db3bf039e37c','cibw0002'),
('2019-10-26 04:35:25','5db3bf8e02420','cib00004'),
('2019-10-26 04:35:40','5db3bf050a8a0','cibw0001'),
('2019-10-26 04:36:34','5db3c6c4465d2','cibw0001'),
('2019-10-26 04:36:46','5db3c6d01f5da','cib00003'),
('2019-10-26 04:37:01','5db3c31f132a2','cibw0003'),
('2019-10-26 04:37:36','5db3c03e7e028','cib00006'),
('2019-10-26 04:37:55','5db3bf050a8a0','cibw0001'),
('2019-10-26 04:37:56','5db3bf039e37c','cib00007'),
('2019-10-26 04:38:02','5db3c153c2bdb','cibw0002'),
('2019-10-26 04:38:12','5db3c72d39108','cibw0002'),
('2019-10-26 04:38:40','5db3bee967e2c','cib00006'),
('2019-10-26 04:39:26','5db3c6d01f5da','cib00004'),
('2019-10-26 04:39:26','5db3bf48238aa','cib00005'),
('2019-10-26 04:39:54','5db3c72d39108','cibw0003'),
('2019-10-26 04:40:43','5db3bf039e37c','cibw0003'),
('2019-10-26 04:41:07','5db3c03e7e028','cib00007'),
('2019-10-26 04:41:50','5db3bee967e2c','cib00007'),
('2019-10-26 04:42:57','5db3bf48238aa','cibw0002'),
('2019-10-26 04:43:00','5db3c72d39108','cibw0003'),
('2019-10-26 04:43:18','5db3c6c4465d2','cib00003'),
('2019-10-26 04:44:32','5db3bee967e2c','cibw0003'),
('2019-10-26 04:44:48','5db3c6d01f5da','cib00005'),
('2019-10-26 04:44:52','5db3bf8e02420','cib00005'),
('2019-10-26 04:44:54','5db3c03e7e028','cibw0003'),
('2019-10-26 04:45:35','5db3c567451b2','cib00003'),
('2019-10-26 04:47:11','5db3bfd923aac','cib00006'),
('2019-10-26 04:47:46','5db3c153c2bdb','cib00006'),
('2019-10-26 04:47:52','5db3c291da349','cib00006'),
('2019-10-26 04:48:42','5db3bf8e02420','cibw0002'),
('2019-10-26 04:48:48','5db3c6d01f5da','cibw0002'),
('2019-10-26 04:49:53','5db3c291da349','cib00007'),
('2019-10-26 04:50:27','5db3c153c2bdb','cib00007'),
('2019-10-26 04:50:43','5db3c567451b2','cib00004'),
('2019-10-26 04:52:23','5db3c72d39108','cib00003'),
('2019-10-26 04:52:24','5db3c291da349','cibw0003'),
('2019-10-26 04:53:22','5db3c4da5de19','cib00003'),
('2019-10-26 04:54:04','5db3c153c2bdb','cibw0003'),
('2019-10-26 04:54:15','5db3c4da5de19','cib00003'),
('2019-10-26 04:55:09','5db3c72d39108','cib00004'),
('2019-10-26 04:55:48','5db3c72d39108','cib00005'),
('2019-10-26 04:55:53','5db3c567451b2','cib00005'),
('2019-10-26 04:57:02','5db3c4da5de19','cib00004'),
('2019-10-26 04:57:45','5db3bfd923aac','cib00007'),
('2019-10-26 04:59:33','5db3c4da5de19','cib00005'),
('2019-10-26 05:00:08','5db3bf48238aa','cib00006'),
('2019-10-26 05:00:11','5db3c6d01f5da','cib00006'),
('2019-10-26 05:00:14','5db3c6c4465d2','cib00004'),
('2019-10-26 05:00:20','5db3c567451b2','cibw0002'),
('2019-10-26 05:00:44','5db3bfd923aac','cibw0003'),
('2019-10-26 05:02:33','5db3c4da5de19','cibw0002'),
('2019-10-26 05:02:57','5db3bfd923aac','cibw0003'),
('2019-10-26 05:03:15','5db3c6d01f5da','cib00007'),
('2019-10-26 05:03:27','5db3bf48238aa','cib00007'),
('2019-10-26 05:03:29','5db3bf8e02420','cib00006'),
('2019-10-26 05:03:31','5db3c6c4465d2','cib00005'),
('2019-10-26 05:05:06','5db3bfd923aac','cibw0003'),
('2019-10-26 05:05:16','5db3c6c4465d2','cibw0002'),
('2019-10-26 05:05:54','5db3bf8e02420','cib00007'),
('2019-10-26 05:06:36','5db3bf48238aa','cibw0003'),
('2019-10-26 05:08:43','5db3c6d01f5da','cibw0003'),
('2019-10-26 05:08:47','5db3bf8e02420','cibw0003'),
('2019-10-26 05:12:30','5db3c4da5de19','cib00006'),
('2019-10-26 05:13:19','5db3c6c4465d2','cib00006'),
('2019-10-26 05:13:53','5db3c4da5de19','cib00007'),
('2019-10-26 05:15:35','5db3c6c4465d2','cib00007'),
('2019-10-26 05:16:06','5db3c4da5de19','cibw0003'),
('2019-10-26 05:17:39','5db3c6c4465d2','cibw0003'),
('2019-10-26 05:18:05','5db3c72d39108','cib00006'),
('2019-10-26 05:19:24','5db3c567451b2','cib00006'),
('2019-10-26 05:20:25','5db3c72d39108','cib00007'),
('2019-10-26 05:23:30','5db3c567451b2','cib00007'),
('2019-10-26 05:26:04','5db3c567451b2','cibw0003'),
('2019-10-26 05:32:00','5db3da50332ff','cib00000'),
('2019-10-26 13:30:41','5db44a811aa24','cib00000'),
('2019-10-26 13:43:32','5db44a811aa24','cib00001'),
('2019-10-26 13:57:49','5db44a811aa24','cib00002'),
('2019-10-26 14:05:36','5db44a811aa24','cibw0001'),
('2019-10-26 14:24:19','5db44a811aa24','cib00003'),
('2019-10-26 14:28:16','5db44a811aa24','cib00004'),
('2019-10-26 14:41:21','5db44a811aa24','cib00005'),
('2019-10-26 14:47:43','5db44a811aa24','cibw0002'),
('2019-10-26 15:24:03','5db44a811aa24','cib00006'),
('2019-10-26 15:55:41','5db44a811aa24','cib00007'),
('2019-10-26 15:59:20','5db44a811aa24','cibw0003'),
('2019-10-28 10:31:45','5db6c39117257','cib00000'),
('2019-10-28 10:42:22','5db6c39117257','cib00001'),
('2019-10-28 10:52:21','5db6c39117257','cib00002'),
('2019-10-28 10:58:39','5db6c39117257','cibw0001'),
('2019-10-28 11:23:18','5db6c39117257','cib00003'),
('2019-10-28 11:30:35','5db6c39117257','cib00004'),
('2019-10-28 11:45:45','5db6c39117257','cib00005'),
('2019-10-28 11:50:42','5db6c39117257','cibw0002'),
('2019-10-28 12:21:12','5db6c39117257','cib00006'),
('2019-10-28 12:27:05','5db6c39117257','cib00007'),
('2019-10-28 12:30:48','5db6c39117257','cibw0003');
/*!40000 ALTER TABLE `ACTIVITY_LOGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROGRESS`
--

DROP TABLE IF EXISTS `PROGRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROGRESS` (
  `user_ID` varchar(15) NOT NULL,
  `topic_num` int(11) NOT NULL,
  `curr_slide` int(11) DEFAULT '0',
  `percentage` float DEFAULT '0',
  `notes` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`user_ID`,`topic_num`),
  KEY `topic_num` (`topic_num`),
  CONSTRAINT `PROGRESS_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `USER_PROFILES` (`user_ID`),
  CONSTRAINT `PROGRESS_ibfk_2` FOREIGN KEY (`topic_num`) REFERENCES `TOPICS` (`topic_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROGRESS`
--

LOCK TABLES `PROGRESS` WRITE;
/*!40000 ALTER TABLE `PROGRESS` DISABLE KEYS */;
INSERT INTO `PROGRESS` VALUES ('5d01dabf6e58c',1,15,1,'Notes for first topic'),('5d01dabf6e58c',2,11,1,NULL),('5d01dabf6e58c',3,1,1,NULL),('5d01dabf6e58c',4,8,1,NULL),('5d01dabf6e58c',5,13,0.928571,NULL),('5d01dabf6e58c',6,20,1,NULL),('5d01dabf6e58c',7,10,1,NULL),('5da9fabee46fa',1,14,0.933333,NULL),('5da9fabee46fa',2,0,0,NULL),('5da9fabee46fa',3,0,0,NULL),('5da9fabee46fa',4,0,0,NULL),('5da9fabee46fa',5,0,0,NULL),('5da9fabee46fa',6,0,0,NULL),('5da9fabee46fa',7,0,0,NULL),('5da9fdda1b5e0',1,15,1,NULL),('5da9fdda1b5e0',2,1,0,NULL),('5da9fdda1b5e0',3,0,0,NULL),('5da9fdda1b5e0',4,7,0.875,NULL),('5da9fdda1b5e0',5,0,0,NULL),('5da9fdda1b5e0',6,0,0,NULL),('5da9fdda1b5e0',7,0,0,NULL),('5daa5c5d0e58b',1,15,1,NULL),('5daa5c5d0e58b',2,11,1,NULL),('5daa5c5d0e58b',3,18,1,NULL),('5daa5c5d0e58b',4,8,1,NULL),('5daa5c5d0e58b',5,13,0.928571,NULL),('5daa5c5d0e58b',6,0,0,NULL),('5daa5c5d0e58b',7,0,0,NULL),('5daa5c8461a26',1,15,1,NULL),('5daa5c8461a26',2,11,1,NULL),('5daa5c8461a26',3,18,1,NULL),('5daa5c8461a26',4,8,1,NULL),('5daa5c8461a26',5,14,1,NULL),('5daa5c8461a26',6,20,1,NULL),('5daa5c8461a26',7,10,1,NULL),('5daa5c8da4475',1,15,1,NULL),('5daa5c8da4475',2,11,1,NULL),('5daa5c8da4475',3,18,1,NULL),('5daa5c8da4475',4,8,1,NULL),('5daa5c8da4475',5,14,1,NULL),('5daa5c8da4475',6,20,1,NULL),('5daa5c8da4475',7,10,1,NULL),('5daa5c972a89e',1,15,1,NULL),('5daa5c972a89e',2,11,1,NULL),('5daa5c972a89e',3,18,1,NULL),('5daa5c972a89e',4,8,1,NULL),('5daa5c972a89e',5,13,0.928571,NULL),('5daa5c972a89e',6,0,0,NULL),('5daa5c972a89e',7,0,0,NULL),('5daa730edeedf',1,15,1,NULL),('5daa730edeedf',2,11,1,NULL),('5daa730edeedf',3,18,1,NULL),('5daa730edeedf',4,8,1,NULL),('5daa730edeedf',5,14,1,NULL),('5daa730edeedf',6,20,1,NULL),('5daa730edeedf',7,10,1,NULL),('5daa73334e4d8',1,15,1,NULL),('5daa73334e4d8',2,11,1,NULL),('5daa73334e4d8',3,18,1,NULL),('5daa73334e4d8',4,8,1,NULL),('5daa73334e4d8',5,14,1,NULL),('5daa73334e4d8',6,20,1,NULL),('5daa73334e4d8',7,10,1,NULL),('5daa74d3ab3d9',1,15,1,NULL),('5daa74d3ab3d9',2,11,1,NULL),('5daa74d3ab3d9',3,18,1,NULL),('5daa74d3ab3d9',4,8,1,NULL),('5daa74d3ab3d9',5,14,1,NULL),('5daa74d3ab3d9',6,20,1,NULL),('5daa74d3ab3d9',7,10,1,NULL),('5daa74d6ceb56',1,15,1,NULL),('5daa74d6ceb56',2,11,1,NULL),('5daa74d6ceb56',3,18,1,NULL),('5daa74d6ceb56',4,8,1,NULL),('5daa74d6ceb56',5,14,1,NULL),('5daa74d6ceb56',6,20,1,NULL),('5daa74d6ceb56',7,10,1,NULL),('5daa76517e866',1,15,1,NULL),('5daa76517e866',2,11,1,NULL),('5daa76517e866',3,18,1,NULL),('5daa76517e866',4,8,1,NULL),('5daa76517e866',5,14,1,NULL),('5daa76517e866',6,20,1,NULL),('5daa76517e866',7,10,1,NULL),('5daa76e823818',1,15,1,NULL),('5daa76e823818',2,11,1,NULL),('5daa76e823818',3,18,1,NULL),('5daa76e823818',4,8,1,NULL),('5daa76e823818',5,14,1,NULL),('5daa76e823818',6,20,1,NULL),('5daa76e823818',7,10,1,NULL),('5daa7713be2c2',1,15,1,NULL),('5daa7713be2c2',2,11,1,NULL),('5daa7713be2c2',3,18,1,NULL),('5daa7713be2c2',4,8,1,NULL),('5daa7713be2c2',5,14,1,NULL),('5daa7713be2c2',6,20,1,NULL),('5daa7713be2c2',7,10,1,NULL),('5daa7fd399474',1,15,1,NULL),('5daa7fd399474',2,11,1,NULL),('5daa7fd399474',3,18,1,NULL),('5daa7fd399474',4,8,1,NULL),('5daa7fd399474',5,14,1,NULL),('5daa7fd399474',6,20,1,NULL),('5daa7fd399474',7,10,1,NULL),('5db11dd0ee82f',1,15,1,NULL),('5db11dd0ee82f',2,11,1,NULL),('5db11dd0ee82f',3,18,1,NULL),('5db11dd0ee82f',4,8,1,NULL),('5db11dd0ee82f',5,14,1,NULL),('5db11dd0ee82f',6,20,1,NULL),('5db11dd0ee82f',7,10,1,NULL),('5db3bee967e2c',1,15,1,NULL),('5db3bee967e2c',2,11,1,NULL),('5db3bee967e2c',3,18,1,NULL),('5db3bee967e2c',4,8,1,NULL),('5db3bee967e2c',5,14,1,NULL),('5db3bee967e2c',6,20,1,NULL),('5db3bee967e2c',7,10,1,NULL),('5db3bf039e37c',1,15,1,NULL),('5db3bf039e37c',2,11,1,NULL),('5db3bf039e37c',3,18,1,NULL),('5db3bf039e37c',4,8,1,NULL),('5db3bf039e37c',5,14,1,NULL),('5db3bf039e37c',6,20,1,NULL),('5db3bf039e37c',7,10,1,NULL),('5db3bf050a8a0',1,15,1,NULL),('5db3bf050a8a0',2,11,1,NULL),('5db3bf050a8a0',3,18,1,NULL),('5db3bf050a8a0',4,8,1,NULL),('5db3bf050a8a0',5,14,1,NULL),('5db3bf050a8a0',6,20,1,NULL),('5db3bf050a8a0',7,10,1,NULL),('5db3bf48238aa',1,15,1,NULL),('5db3bf48238aa',2,11,1,NULL),('5db3bf48238aa',3,18,1,NULL),('5db3bf48238aa',4,8,1,NULL),('5db3bf48238aa',5,14,1,NULL),('5db3bf48238aa',6,20,1,NULL),('5db3bf48238aa',7,10,1,NULL),('5db3bf8e02420',1,15,1,NULL),('5db3bf8e02420',2,11,1,NULL),('5db3bf8e02420',3,18,1,NULL),('5db3bf8e02420',4,8,1,NULL),('5db3bf8e02420',5,14,1,NULL),('5db3bf8e02420',6,20,1,NULL),('5db3bf8e02420',7,10,1,NULL),('5db3bfd923aac',1,15,1,NULL),('5db3bfd923aac',2,11,1,NULL),('5db3bfd923aac',3,18,1,NULL),('5db3bfd923aac',4,8,1,NULL),('5db3bfd923aac',5,14,1,NULL),('5db3bfd923aac',6,20,1,NULL),('5db3bfd923aac',7,10,1,NULL),('5db3c03e7e028',1,15,1,'I think that the Syntax is also case sensitive and grammar sensitive'),('5db3c03e7e028',2,11,1,NULL),('5db3c03e7e028',3,18,1,NULL),('5db3c03e7e028',4,8,1,NULL),('5db3c03e7e028',5,14,1,NULL),('5db3c03e7e028',6,20,1,NULL),('5db3c03e7e028',7,10,1,NULL),('5db3c153c2bdb',1,15,1,NULL),('5db3c153c2bdb',2,11,1,NULL),('5db3c153c2bdb',3,18,1,NULL),('5db3c153c2bdb',4,8,1,NULL),('5db3c153c2bdb',5,14,1,NULL),('5db3c153c2bdb',6,20,1,NULL),('5db3c153c2bdb',7,10,1,NULL),('5db3c291da349',1,15,1,NULL),('5db3c291da349',2,11,1,NULL),('5db3c291da349',3,18,1,NULL),('5db3c291da349',4,8,1,NULL),('5db3c291da349',5,14,1,NULL),('5db3c291da349',6,20,1,NULL),('5db3c291da349',7,10,1,NULL),('5db3c31f132a2',1,15,1,NULL),('5db3c31f132a2',2,11,1,NULL),('5db3c31f132a2',3,18,1,NULL),('5db3c31f132a2',4,8,1,NULL),('5db3c31f132a2',5,14,1,NULL),('5db3c31f132a2',6,20,1,NULL),('5db3c31f132a2',7,10,1,NULL),('5db3c4da5de19',1,15,1,NULL),('5db3c4da5de19',2,11,1,NULL),('5db3c4da5de19',3,18,1,NULL),('5db3c4da5de19',4,8,1,NULL),('5db3c4da5de19',5,14,1,NULL),('5db3c4da5de19',6,20,1,NULL),('5db3c4da5de19',7,10,1,NULL),('5db3c567451b2',1,15,1,NULL),('5db3c567451b2',2,11,1,NULL),('5db3c567451b2',3,18,1,NULL),('5db3c567451b2',4,8,1,NULL),('5db3c567451b2',5,14,1,NULL),('5db3c567451b2',6,20,1,NULL),('5db3c567451b2',7,10,1,NULL),('5db3c6c4465d2',1,15,1,NULL),('5db3c6c4465d2',2,11,1,NULL),('5db3c6c4465d2',3,18,1,NULL),('5db3c6c4465d2',4,8,1,NULL),('5db3c6c4465d2',5,14,1,NULL),('5db3c6c4465d2',6,20,1,NULL),('5db3c6c4465d2',7,10,1,NULL),('5db3c6d01f5da',1,15,1,NULL),('5db3c6d01f5da',2,11,1,NULL),('5db3c6d01f5da',3,18,1,NULL),('5db3c6d01f5da',4,8,1,NULL),('5db3c6d01f5da',5,14,1,NULL),('5db3c6d01f5da',6,20,1,NULL),('5db3c6d01f5da',7,10,1,NULL),('5db3c72d39108',1,15,1,NULL),('5db3c72d39108',2,11,1,NULL),('5db3c72d39108',3,18,1,NULL),('5db3c72d39108',4,8,1,NULL),('5db3c72d39108',5,14,1,NULL),('5db3c72d39108',6,20,1,NULL),('5db3c72d39108',7,10,1,NULL),('5db3da50332ff',1,14,0.933333,NULL),('5db3da50332ff',2,0,0,NULL),('5db3da50332ff',3,0,0,NULL),('5db3da50332ff',4,0,0,NULL),('5db3da50332ff',5,0,0,NULL),('5db3da50332ff',6,0,0,NULL),('5db3da50332ff',7,0,0,NULL),('5db44a811aa24',1,15,1,NULL),('5db44a811aa24',2,11,1,NULL),('5db44a811aa24',3,18,1,NULL),('5db44a811aa24',4,8,1,NULL),('5db44a811aa24',5,14,1,NULL),('5db44a811aa24',6,20,1,NULL),('5db44a811aa24',7,10,1,NULL),('5db6c39117257',1,15,1,NULL),('5db6c39117257',2,11,1,NULL),('5db6c39117257',3,18,1,NULL),('5db6c39117257',4,8,1,NULL),('5db6c39117257',5,14,1,NULL),('5db6c39117257',6,20,1,NULL),('5db6c39117257',7,10,1,NULL);
/*!40000 ALTER TABLE `PROGRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUESTIONS`
--

DROP TABLE IF EXISTS `QUESTIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUESTIONS` (
  `question_ID` varchar(15) NOT NULL,
  `sheet_num` int(11) NOT NULL,
  `question` text NOT NULL,
  `option_A` text,
  `option_B` text,
  `option_C` text,
  `Answer` varchar(500) NOT NULL,
  PRIMARY KEY (`question_ID`),
  KEY `sheet_num` (`sheet_num`),
  CONSTRAINT `QUESTIONS_ibfk_1` FOREIGN KEY (`sheet_num`) REFERENCES `WORKSHEETS` (`sheet_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUESTIONS`
--

LOCK TABLES `QUESTIONS` WRITE;
/*!40000 ALTER TABLE `QUESTIONS` DISABLE KEYS */;
INSERT INTO `QUESTIONS` VALUES ('5d041d0166b48',1,'Accessing a variable is not case-sensitive. Suppose you have a variable called \'month\', it can be accessed by using month, Month, or MONTH.','True','False',NULL,'B'),('5d041d0166b4e',1,'Programming Languages are tools used to _____________.','translate natural language to machine language','create computer programs','modify computer settings','B'),('5d041d0166b4f',1,'What is computer programming?','Speeding up your computer.','Building a computer.','Telling the computer what to do through a special set of instructions.','C'),('5d041d0166b50',1,'Like the English language, a Programming Language also has words, symbols and grammatical rules.','True','False',NULL,'A'),('5d041d0166b51',1,'_____________ refers to the correct ‘spelling’ and ‘grammar’ of a Programming Language.','Syntax','Source Code','Machine Language','A'),('5d041d0166b52',1,'_____________is a person who writes computer programs.','Programmer','Coder','Hacker','A'),('5d041d0166b53',1,'Humans are capable of expressing their thoughts in 1’s and 0’s.','True','False',NULL,'B'),('5d041d0166b54',1,'How does programming work?','Machine Code -> Compiling -> Source Code','Compiling -> Source Code -> Machine Code','Source Code -> Compiling -> Machine Code','C'),('5d041d0166b55',1,'_____________ are the list of predefined words or reserved words used in programming languages that cannot be used as identifiers.','Keywords','Operators','None of the above.','A'),('5d041d0166b56',1,'Given the code snippet below, what will be the output of the program?<p class=\'snippet\'>print(‘Hello\')<br>print(\'world’)</p>','<p class=\'snippet\'>Hello world</p>','<p class=\'snippet\'>HelloWord</p>','<p class=\'snippet\'>Hello<br>world</p>','C'),('5d041d0166b58',1,'Identifiers cannot have special characters in them.','True','False',NULL,'A'),('5d041d0166b59',1,'Keywords can be used as variable identifiers.','True','False',NULL,'B'),('5d041d0166b5a',1,'In Python, which of the following signs is used to assign values to variables?','==','->','=','C'),('5d041d0166b5b',1,'Suppose you have this variable assignment, what is the value of the y variable?<p class=\'snippet\'>x = 50</p><p class=\'snippet\'>y = 15</p>','15','50','5','A'),('5d041d0166b5c',1,'Which of the following is a valid identifier?','print','_22','1one','B'),('5d2dd6917581e',1,'Display “Hello World” using print. Give the proper python syntax.',NULL,NULL,NULL,'print|Hello|World'),('5d76c5623fdb4',1,'How do you assign a value to a variable? Give the proper python syntax.',NULL,NULL,NULL,'variable|=|value'),('5d76c5623fdba',1,'Create a variable called \'name\'. Assign the text \'Guido\' to the variable created. Then, display the value of the variable',NULL,NULL,NULL,'name|=|Guido|print|name'),('5d7749871a1be',2,'Which of the following is NOT a category of operators in python?','Logical','Rational','Arithmetic','B'),('5d7749871a1c1',2,'_____________ is a mathematical function that computes two operands.','Relational Operators','Assignment Operators','Arithmetic Operators','C'),('5d7749871a1c2',2,'In python, what symbol should you use if you want to perform division?','÷','/','&','B'),('5d7749871a1c3',2,'What is the equivalent of 7 raised to 2 in python?','7 ** 2','7 ^ 2','7 * 2','A'),('5d7749871a1c4',2,'In Python, what is the correct arithmetic precedence?<table class=\'table table-bordered text-center\'><tbody><tr><td>A - Addition</td><td>E - Exponential</td></tr><tr><td>D - Division</td><td>M - Multiplication</td></tr><tr><td>P - Parentheses</td><td>S - Subtraction</td></tr></tbody></table>','PESMAD','PEMSAD','PEMDAS','C'),('5d7749871a1c5',2,'Given this code snippet,<p class=\'snippet\'>test = 2 * 3 - 1 + 2</p>What will be the value of test?','7','6','3','A'),('5d7749871a1c6',2,'Variables can be used to store the result of a mathematical operation.','True','False',NULL,'A'),('5d7749871a1c7',2,'Given the following code, is the value of num1 the same with num2?<p class=\'snippet\'>num1 = (20 - 5 / 3)<br>num2 = ((20 - 5) / 3)</p>','True','False',NULL,'A'),('5d7749871a1c8',2,'Which of the following has the correct python syntax for variable assignment?','1 + 1 = sum','sum = 1 + 1','sum = 1 + 1 = sum','B'),('5d7749871a1c9',2,'Create a Python program that multiplies 2 and 10. Store the result of a variable named \'product\'.',NULL,NULL,NULL,'product|=|*'),('5d7749871a1ca',2,'These symbols are used to compare two values.','Relational Operators','Assignment Operators','Arithmetic Operators','A'),('5d7749871a1cc',2,'In python what symbol is used if the variable on the left is less than or equal to the right variable?','==','>=','<=','C'),('5d7749871a1cf',2,'Given the following code snippet, what will be the output of the program?<p class=\'snippet\'>num = 5<br>if num == 5:<br>&nbsp;&nbsp;print(\'The number is 5\')</p>','5','\'The number is 5\'','The number is 5','C'),('5d7749871a1d0',2,'Which of the following is the correct Python syntax for an if-else statement?','<p class=\'snippet\'>if condition:<br>&nbsp;&nbsp;statements/expressions<br>else:<br>&nbsp;&nbsp;statements/expressions</p>','<p class=\'snippet\'>if statement then:<br>&nbsp;&nbsp;statements/expressions<br>else:<br>&nbsp;&nbsp;statements/expressions</p>','None of the above','A'),('5d7749871a1d1',2,'Given the following code snippet, what will be the output of the program?<p class=\'snippet\'>age = 18<br>if age < 12:<br>&nbsp;&nbsp;print(\'Child\')<br>else:<br>&nbsp;&nbsp;print(\'Adult\')</p>','Child','Adult','Error','B'),('5d775314f1204',3,'OR, AND and NOT are __________ operators?','Arithmetic','Logical','Relational','B'),('5d775314f1207',3,'The AND operator returns False when atleast one of the operands is False.','True','False',NULL,'A'),('5d775314f1208',3,'Given this logical expression<p class=\'snippet\'>(1 = 1) OR (5 >= 8)</p>what will be the value of the expression?','True','False','Error','A'),('5d775314f1209',3,'This logical operator returns True if atleast one of the operands is true.','NOT','OR','AND','B'),('5d775314f120a',3,'Suppose you have this logical expression<p class=\'snippet\'>(1 > 0) AND (0 < 1)</p>what will be the value of the expression?','True','False','Error','A'),('5d775314f120b',3,'Given the following code snippet, what will be the value of \'isMarried\'?<p class=\'snippet\'>isMarried = true<br>isMarried = not isMarried</p>','True','False',NULL,'B'),('5d775314f120c',3,'Suppose you have this logical expression, <p class=\'snippet\'>4 <= 4</p>what will be the value of the expression?','True','False',NULL,'A'),('5d775314f120d',3,'_____________ is a sequence of instructions that is continuously repeated until a certain condition is satisfied.','Iterative Statements','Loops','All of the above','C'),('5d775314f120e',3,'For Loop is an example of an iterative statement.','True','False',NULL,'A'),('5d775314f120f',3,'In the execution of a For-Loop, how many parts does it have?','2','3','4','B'),('5d775314f1210',3,'Given the following code snippet, what does n represents?<p class=\'snippet\'>for count in range(n):</p>','Number of random values','Number of output','Number of iterations','C'),('5d775314f1211',3,'Create a program that will output “I love CodeInBlocks” 10 times.',NULL,NULL,NULL,'for|in|range|10|print|I love CodeInBlocks');
/*!40000 ALTER TABLE `QUESTIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SESSIONS`
--

DROP TABLE IF EXISTS `SESSIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SESSIONS` (
  `sheet_num` int(11) NOT NULL,
  `user_ID` varchar(15) NOT NULL,
  `score` int(11) DEFAULT '0',
  KEY `sheet_num` (`sheet_num`),
  KEY `user_ID` (`user_ID`),
  CONSTRAINT `SESSIONS_ibfk_1` FOREIGN KEY (`sheet_num`) REFERENCES `WORKSHEETS` (`sheet_num`),
  CONSTRAINT `SESSIONS_ibfk_2` FOREIGN KEY (`user_ID`) REFERENCES `USER_PROFILES` (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SESSIONS`
--

LOCK TABLES `SESSIONS` WRITE;
/*!40000 ALTER TABLE `SESSIONS` DISABLE KEYS */;
INSERT INTO `SESSIONS` VALUES (1,'5d01dabf6e58c',6),(2,'5d01dabf6e58c',6),(3,'5d01dabf6e58c',5),(1,'5da9fabee46fa',0),(2,'5da9fabee46fa',0),(3,'5da9fabee46fa',0),(1,'5da9fdda1b5e0',0),(2,'5da9fdda1b5e0',0),(3,'5da9fdda1b5e0',0),(1,'5daa5c5d0e58b',12),(2,'5daa5c5d0e58b',0),(3,'5daa5c5d0e58b',0),(1,'5daa5c8461a26',11),(2,'5daa5c8461a26',9),(3,'5daa5c8461a26',6),(1,'5daa5c8da4475',9),(2,'5daa5c8da4475',11),(3,'5daa5c8da4475',3),(1,'5daa5c972a89e',9),(2,'5daa5c972a89e',0),(3,'5daa5c972a89e',0),(1,'5daa730edeedf',8),(2,'5daa730edeedf',13),(3,'5daa730edeedf',7),(1,'5daa73334e4d8',13),(2,'5daa73334e4d8',13),(3,'5daa73334e4d8',10),(1,'5daa74d3ab3d9',13),(2,'5daa74d3ab3d9',10),(3,'5daa74d3ab3d9',8),(1,'5daa74d6ceb56',8),(2,'5daa74d6ceb56',10),(3,'5daa74d6ceb56',7),(1,'5daa76517e866',11),(2,'5daa76517e866',10),(3,'5daa76517e866',7),(1,'5daa76e823818',12),(2,'5daa76e823818',8),(3,'5daa76e823818',5),(1,'5daa7713be2c2',11),(2,'5daa7713be2c2',9),(3,'5daa7713be2c2',3),(1,'5daa7fd399474',13),(2,'5daa7fd399474',12),(3,'5daa7fd399474',4),(1,'5db11dd0ee82f',14),(2,'5db11dd0ee82f',8),(3,'5db11dd0ee82f',9),(1,'5db3bee967e2c',13),(2,'5db3bee967e2c',13),(3,'5db3bee967e2c',11),(1,'5db3bf039e37c',13),(2,'5db3bf039e37c',14),(3,'5db3bf039e37c',9),(1,'5db3bf050a8a0',17),(2,'5db3bf050a8a0',15),(3,'5db3bf050a8a0',9),(1,'5db3bf48238aa',14),(2,'5db3bf48238aa',14),(3,'5db3bf48238aa',6),(1,'5db3bf8e02420',14),(2,'5db3bf8e02420',12),(3,'5db3bf8e02420',10),(1,'5db3bfd923aac',13),(2,'5db3bfd923aac',12),(3,'5db3bfd923aac',10),(1,'5db3c03e7e028',14),(2,'5db3c03e7e028',14),(3,'5db3c03e7e028',9),(1,'5db3c153c2bdb',16),(2,'5db3c153c2bdb',14),(3,'5db3c153c2bdb',11),(1,'5db3c291da349',13),(2,'5db3c291da349',12),(3,'5db3c291da349',11),(1,'5db3c31f132a2',16),(2,'5db3c31f132a2',14),(3,'5db3c31f132a2',12),(1,'5db3c4da5de19',13),(2,'5db3c4da5de19',14),(3,'5db3c4da5de19',10),(1,'5db3c567451b2',15),(2,'5db3c567451b2',14),(3,'5db3c567451b2',9),(1,'5db3c6c4465d2',14),(2,'5db3c6c4465d2',13),(3,'5db3c6c4465d2',10),(1,'5db3c6d01f5da',14),(2,'5db3c6d01f5da',13),(3,'5db3c6d01f5da',11),(1,'5db3c72d39108',16),(2,'5db3c72d39108',14),(3,'5db3c72d39108',11),(1,'5db3da50332ff',0),(2,'5db3da50332ff',0),(3,'5db3da50332ff',0),(1,'5db44a811aa24',13),(2,'5db44a811aa24',12),(3,'5db44a811aa24',7),(1,'5db6c39117257',15),(2,'5db6c39117257',9),(3,'5db6c39117257',3);
/*!40000 ALTER TABLE `SESSIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SLIDES`
--

DROP TABLE IF EXISTS `SLIDES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SLIDES` (
  `slide_ID` varchar(15) NOT NULL,
  `topic_num` int(11) NOT NULL,
  `slide_num` int(11) NOT NULL,
  `file_path` varchar(100) NOT NULL,
  `is_practice_slide` tinyint(1) NOT NULL,
  `practice_key` text,
  PRIMARY KEY (`slide_ID`),
  KEY `topic_num` (`topic_num`),
  CONSTRAINT `SLIDES_ibfk_1` FOREIGN KEY (`topic_num`) REFERENCES `TOPICS` (`topic_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SLIDES`
--

LOCK TABLES `SLIDES` WRITE;
/*!40000 ALTER TABLE `SLIDES` DISABLE KEYS */;
INSERT INTO `SLIDES` VALUES ('5d0297bf04c2e',1,1,'assets/imgs/slides/topic1/Slide1.PNG',0,NULL),('5d0297f8a9823',1,2,'assets/imgs/slides/topic1/Slide2.PNG',0,NULL),('5d02983293de8',1,3,'assets/imgs/slides/topic1/Slide3.PNG',0,NULL),('5d029846a52f4',1,4,'assets/imgs/slides/topic1/Slide4.PNG',0,NULL),('5d029857d2e5d',1,5,'assets/imgs/slides/topic1/Slide5.PNG',0,NULL),('5d029b03d48e0',1,6,'assets/imgs/slides/topic1/Slide6.PNG',0,NULL),('5d029b1cb65bf',1,7,'assets/imgs/slides/topic1/Slide7.PNG',0,NULL),('5d029b34d15a4',1,8,'assets/imgs/slides/topic1/Slide8.PNG',0,NULL),('5d029b4c3dc01',1,9,'assets/imgs/slides/topic1/Slide9.PNG',0,NULL),('5d029b790d178',1,10,'assets/imgs/slides/topic1/Slide10.gif',0,NULL),('5d029ba2f2165',1,11,'assets/imgs/slides/topic1/Slide11.PNG',0,NULL),('5d029bc08d793',1,12,'assets/imgs/slides/topic1/Slide12.PNG',0,NULL),('5d029bf48ecaf',1,13,'assets/imgs/slides/topic1/Slide13.PNG',0,NULL),('5d029bfef40c7',1,14,'assets/imgs/slides/topic1/Slide14.PNG',1,'print|John|print|2019|print|I|am|using|Blockly~John2019I am using Blockly.'),('5d029c077c4fc',1,15,'assets/imgs/slides/topic1/Slide15.gif',0,NULL),('5d7327ff6e869',2,1,'assets/imgs/slides/topic2/Slide16.png',0,NULL),('5d7327ff6e86e',2,2,'assets/imgs/slides/topic2/Slide17.png',0,NULL),('5d7327ff6e86f',2,3,'assets/imgs/slides/topic2/Slide18.png',0,NULL),('5d7327ff6e870',2,4,'assets/imgs/slides/topic2/Slide19.png',0,NULL),('5d7327ff6e871',2,5,'assets/imgs/slides/topic2/Slide20.png',0,NULL),('5d7327ff6e872',2,6,'assets/imgs/slides/topic2/Slide21.png',0,NULL),('5d7327ff6e873',2,7,'assets/imgs/slides/topic2/Slide22.png',0,NULL),('5d7327ff6e874',2,8,'assets/imgs/slides/topic2/Slide23.gif',0,NULL),('5d7327ff6e875',2,9,'assets/imgs/slides/topic2/Slide24.png',0,NULL),('5d7327ff6e876',2,10,'assets/imgs/slides/topic2/Slide25.png',1,'=|Steve|=|1955|=|P~Steve1955P'),('5d73287de2687',2,11,'assets/imgs/slides/topic2/Slide26.gif',0,NULL),('5d7345269c963',3,1,'assets/imgs/slides/topic3/Slide27.png',0,NULL),('5d7345269c966',3,2,'assets/imgs/slides/topic3/Slide28.png',0,NULL),('5d7345269c967',3,3,'assets/imgs/slides/topic3/Slide29.png',0,NULL),('5d7345269c968',3,4,'assets/imgs/slides/topic3/Slide30.png',0,NULL),('5d7345269c969',3,5,'assets/imgs/slides/topic3/Slide31.png',0,NULL),('5d7345269c96a',3,6,'assets/imgs/slides/topic3/Slide32.png',0,NULL),('5d7345269c96b',3,7,'assets/imgs/slides/topic3/Slide33.png',0,NULL),('5d7345269c96c',3,8,'assets/imgs/slides/topic3/Slide34.gif',0,NULL),('5d7345269c96d',3,9,'assets/imgs/slides/topic3/Slide35.png',1,'print|*~4050'),('5d7345269c96e',3,10,'assets/imgs/slides/topic3/Slide36.gif',0,NULL),('5d7345269c96f',3,11,'assets/imgs/slides/topic3/Slide37.png',0,NULL),('5d7345269c970',3,12,'assets/imgs/slides/topic3/Slide38.png',1,'=|=|+|print~22'),('5d7345269c971',3,13,'assets/imgs/slides/topic3/Slide39.gif',0,NULL),('5d7345269c972',3,14,'assets/imgs/slides/topic3/Slide40.png',0,NULL),('5d7345269c973',3,15,'assets/imgs/slides/topic3/Slide41.png',0,NULL),('5d7345269c974',3,16,'assets/imgs/slides/topic3/Slide42.png',0,NULL),('5d7345269c975',3,17,'assets/imgs/slides/topic3/Slide43.png',1,'print|*|-|**~24'),('5d7345269c976',3,18,'assets/imgs/slides/topic3/Slide44.gif',0,NULL),('5d734752ac6ce',4,1,'assets/imgs/slides/topic4/Slide45.png',0,NULL),('5d734752ac6d1',4,2,'assets/imgs/slides/topic4/Slide46.png',0,NULL),('5d734752ac6d2',4,3,'assets/imgs/slides/topic4/Slide47.png',0,NULL),('5d734752ac6d3',4,4,'assets/imgs/slides/topic4/Slide48.png',0,NULL),('5d734752ac6d4',4,5,'assets/imgs/slides/topic4/Slide49.png',0,NULL),('5d734752ac6d5',4,6,'assets/imgs/slides/topic4/Slide50.gif',0,NULL),('5d734752ac6d6',4,7,'assets/imgs/slides/topic4/Slide51.png',1,'print|/|==~true'),('5d734752ac6d7',4,8,'assets/imgs/slides/topic4/Slide52.gif',0,NULL),('5d7347fce2a24',5,1,'assets/imgs/slides/topic5/Slide53.png',0,NULL),('5d7347fce2a27',5,2,'assets/imgs/slides/topic5/Slide54.png',0,NULL),('5d7347fce2a28',5,3,'assets/imgs/slides/topic5/Slide55.png',0,NULL),('5d7347fce2a29',5,4,'assets/imgs/slides/topic5/Slide56.png',0,NULL),('5d7347fce2a2a',5,5,'assets/imgs/slides/topic5/Slide57.png',0,NULL),('5d7347fce2a2b',5,6,'assets/imgs/slides/topic5/Slide58.png',0,NULL),('5d7347fce2a2c',5,7,'assets/imgs/slides/topic5/Slide59.gif',0,NULL),('5d7347fce2a2d',5,8,'assets/imgs/slides/topic5/Slide60.png',0,NULL),('5d7347fce2a2e',5,9,'assets/imgs/slides/topic5/Slide61.png',0,NULL),('5d7347fce2a2f',5,10,'assets/imgs/slides/topic5/Slide62.png',0,NULL),('5d7347fce2a30',5,11,'assets/imgs/slides/topic5/Slide63.png',0,NULL),('5d7347fce2a31',5,12,'assets/imgs/slides/topic5/Slide64.gif',0,NULL),('5d7347fce2a32',5,13,'assets/imgs/slides/topic5/Slide65.png',1,'if|&|gt|;|print|else|print~25'),('5d7347fce2a33',5,14,'assets/imgs/slides/topic5/Slide66.gif',0,NULL),('5d7348f6d7afa',6,1,'assets/imgs/slides/topic6/Slide67.png',0,NULL),('5d7348f6d7afd',6,2,'assets/imgs/slides/topic6/Slide68.png',0,NULL),('5d7348f6d7afe',6,3,'assets/imgs/slides/topic6/Slide69.png',0,NULL),('5d7348f6d7aff',6,4,'assets/imgs/slides/topic6/Slide70.png',0,NULL),('5d7348f6d7b00',6,5,'assets/imgs/slides/topic6/Slide71.gif',0,NULL),('5d7348f6d7b01',6,6,'assets/imgs/slides/topic6/Slide72.png',0,NULL),('5d7348f6d7b02',6,7,'assets/imgs/slides/topic6/Slide73.png',0,NULL),('5d7348f6d7b03',6,8,'assets/imgs/slides/topic6/Slide74.png',0,NULL),('5d7348f6d7b04',6,9,'assets/imgs/slides/topic6/Slide75.png',1,'if|&|lt|;|and|!=|print|else|print~Both conditions are correct'),('5d7348f6d7b05',6,10,'assets/imgs/slides/topic6/Slide76.gif',0,NULL),('5d7348f6d7b06',6,11,'assets/imgs/slides/topic6/Slide77.png',0,NULL),('5d7348f6d7b07',6,12,'assets/imgs/slides/topic6/Slide78.png',0,NULL),('5d7348f6d7b08',6,13,'assets/imgs/slides/topic6/Slide79.png',0,NULL),('5d7348f6d7b09',6,14,'assets/imgs/slides/topic6/Slide80.png',1,'if|==|or|** 2|==|print|else|print~One of conditions is correct'),('5d7348f6d7b0a',6,15,'assets/imgs/slides/topic6/Slide81.gif',0,NULL),('5d7348f6d7b0b',6,16,'assets/imgs/slides/topic6/Slide82.png',0,NULL),('5d7348f6d7b0c',6,17,'assets/imgs/slides/topic6/Slide83.png',0,NULL),('5d7348f6d7b0d',6,18,'assets/imgs/slides/topic6/Slide84.png',0,NULL),('5d7348f6d7b0e',6,19,'assets/imgs/slides/topic6/Slide85.png',1,'if|not|print|else|print~Finished'),('5d7348f6d7b0f',6,20,'assets/imgs/slides/topic6/Slide86.gif',0,NULL),('5d7349aeae0f0',7,1,'assets/imgs/slides/topic7/Slide87.png',0,NULL),('5d7349aeae0f3',7,2,'assets/imgs/slides/topic7/Slide88.png',0,NULL),('5d7349aeae0f4',7,3,'assets/imgs/slides/topic7/Slide89.png',0,NULL),('5d7349aeae0f5',7,4,'assets/imgs/slides/topic7/Slide90.png',0,NULL),('5d7349aeae0f6',7,5,'assets/imgs/slides/topic7/Slide91.png',0,NULL),('5d7349aeae0f7',7,6,'assets/imgs/slides/topic7/Slide92.gif',0,NULL),('5d7349aeae0f8',7,7,'assets/imgs/slides/topic7/Slide93.png',0,NULL),('5d7349aeae0f9',7,8,'assets/imgs/slides/topic7/Slide94.png',0,NULL),('5d7349aeae0fa',7,9,'assets/imgs/slides/topic7/Slide95.png',1,'for|in range|=|+|print~12'),('5d7349aeae0fb',7,10,'assets/imgs/slides/topic7/Slide96.gif',0,NULL);
/*!40000 ALTER TABLE `SLIDES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TOPICS`
--

DROP TABLE IF EXISTS `TOPICS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TOPICS` (
  `topic_num` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `cover_path` text,
  `sheet_after` tinyint(1) DEFAULT '0',
  `sheet_num` int(11) NOT NULL,
  PRIMARY KEY (`topic_num`),
  KEY `sheet_num` (`sheet_num`),
  CONSTRAINT `TOPICS_ibfk_1` FOREIGN KEY (`sheet_num`) REFERENCES `WORKSHEETS` (`sheet_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TOPICS`
--

LOCK TABLES `TOPICS` WRITE;
/*!40000 ALTER TABLE `TOPICS` DISABLE KEYS */;
INSERT INTO `TOPICS` VALUES (1,'The Basics of Programming','','assets/imgs/topic_imgs/Topic-1.png',0,1),(2,'Variables','','assets/imgs/topic_imgs/Topic-2.png',1,1),(3,'Arithmetic Operators','','assets/imgs/topic_imgs/Topic-3.png',0,2),(4,'Relational Operators','','assets/imgs/topic_imgs/Topic-4.png',0,2),(5,'Conditional Statements','','assets/imgs/topic_imgs/Topic-5.png',1,2),(6,'Logical Operators','','assets/imgs/topic_imgs/Topic-6.png',0,3),(7,'Iterative Statements','','assets/imgs/topic_imgs/Topic-7.png',1,3);
/*!40000 ALTER TABLE `TOPICS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_PROFILES`
--

DROP TABLE IF EXISTS `USER_PROFILES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER_PROFILES` (
  `user_ID` varchar(15) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_PROFILES`
--

LOCK TABLES `USER_PROFILES` WRITE;
/*!40000 ALTER TABLE `USER_PROFILES` DISABLE KEYS */;
INSERT INTO `USER_PROFILES` VALUES
('5d01dabf6e58c','Greg','Basera','GregBasera','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8','gbasera@gbox.adnu.edu.ph'),
('5da9fabee46fa','Nico','Caneba','ncaneba','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8','ncaneba@gbox.adnu.edu.ph'),
('5da9fdda1b5e0','Danie ','Bianca','daniebianca','d6fa77ad2c8d3abc1387864c0fa5e0e71e1a7ffb','deynigurl@gmail.com'),
('5daa5c5d0e58b','Jonathan','Versoza','Tan','5cdb49471e890335dccc504161f0b7d0488d96b9','jversoza@gbox.adnu.edu.ph'),
('5daa5c8461a26','Rea Marie','Roxas','iyarxs_','369fcdc41c85bc01af3948bd38b192af0700ed04','rroxas@gbox.adnu.edu.ph'),
('5daa5c8da4475','Geminie','Cabalquinto','cabalquintogem_20','3d36fbe4cc3fce5d32c08851bd142d44ce6eebdf','gcabalquinto@gbox.adnu.edu.ph'),
('5daa5c972a89e','Mark Dominic','Abelinde','mabelinde','0b49565c54ec0b20c2e888c6a6f43ee15c0688fa','mabelinde@gbox.adnu.edu'),
('5daa730edeedf','Regina','Balbalosa','reginabalbalosa','89ac9ce95bededa4bef6e7969b45306d46e7952d','rbalbalosa@gbox.adnu.edu.ph'),
('5daa73334e4d8','Leand','Daluro','ldaluro','d0d63766fceed75ccd47250af3074ddcd2b83267','leanddaluro131@gmail.com'),
('5daa74d3ab3d9','Jericho Antonio ','Consulta','echo','c184317d2d890bbda93be144c0b284b5e745614f','consultaecho00@gmail.com'),
('5daa74d6ceb56','Abegail','Mendoza','abby','419172ea9bb591180dffa60f2bf1daf56dd8c04c','abmendoza@gbox.adnu.edu.ph'),
('5daa76517e866','Shinneth','Montales','shinn_montales','37191a5bc3c181388151a716e0f054a0dfc10fb3','shmontales@gbox.adnu.edu.ph'),
('5daa76e823818','Angelica','Camilo','acamilo','3cd0aee265f76cff0c117ca05448d0b0c53af80f','aicacamilo123@gmail.com'),
('5daa7713be2c2','Theresa Marie','Marquese','TheresaMarquese','80a700fbc51946a4a7e215013a3b1014a71897ec','theresamarquese@gmail.com'),
('5daa7fd399474','Gil','Monserate','mons17','fa78226d0d2f69bf429c9e9ebeb1b2973a267b64','gmonserateiii@gbox.adnu.edu.ph'),
('5db11dd0ee82f','Ramon Dominic','Nobleza','dom','40bd001563085fc35165329ea1ff5c5ecbdbbeef','rnobleza@gbox.adnu.edu.ph'),
('5db3bee967e2c','Jeric Marx','Natividad','alahuakbart','f2dd08ca21871c910b43a5614658d8be15123df1','jmnatividad@gbox.adnu.edu.ph'),
('5db3bf039e37c','Gian Mari','Ramos','g','eeb187f782a2659a0410817daf7b74d8fc6d919f','gmramos@gbox.adnu.edu.ph'),
('5db3bf050a8a0','Vicente Jr','Pillora','jeh028','7ec3cd2e20d8496104683a4274d6be40a40d448e','vpillorajr@gbox.adnu.edu.ph'),
('5db3bf48238aa','Aldrich','Lape','aldrichlape','36d470c61b6387ef3d49936d8e0765059471a368','aalape@gbox.adnu.edu.ph'),
('5db3bf8e02420','Jeremy','Perez','bruce','da7ac0554adb5efc1e4791b0a3b87839f1d6413a','jerperez@gbox.adnu.edu.ph'),
('5db3bfd923aac','Khieyl Eugene','Nicolas','kael','395631edd9f33efaf3a09c22f7047eb42564271c','knicolas@gbox.adnu.eduph'),
('5db3c03e7e028','Lorenzo','Lasam','Enzotic','8ffa6dfc042fd9bc017a72778a8c0ae55315c209','llasam@gbox.adnu.edu.ph'),
('5db3c153c2bdb','Lance Kent','Briones','SatorHHO','3ef2d971b47388e538d1381e76069da65c6fc9d7','lbriones@gbox.adnu.edu.ph'),
('5db3c291da349','Bernard Louie','Estioco','Bernard','1520476808134721703725e49e1bdb179d61155c','bestioco@gbox.adnu.edu.ph'),
('5db3c31f132a2','Michael','Luz','organicalmonds','ccf89947c30a18208841ee4f5198fa7737e400e8','jluz@gbox.adnu.edu.ph'),
('5db3c4da5de19','Gerd ','Jana','gerdiedoo','317400af00318bc595f7b833d46db9ea5369b61d','gjana@gbox.adnu.edu.ph'),
('5db3c567451b2','Russel','Hernandez','march','ba4f092dbc8651f58961cf3dca985370e87b4774','ruhernandez@gbox.adnu.edu.ph'),
('5db3c6c4465d2','Aneglica','Cabalquinto','zhazskf','17ae5525694057e8454cf37e19147026f3253ad8','acabalquinto@gbox.adnu.edu.ph'),
('5db3c6d01f5da','Jeffrey','Oliver','JefFreak','9a14852bdeeaec47d6e8f876dc390b4a40b011d6','jeoliver@gbox.adnu.edu.ph'),
('5db3c72d39108','Jon Arielm','Maravilla','jonarielm','7c4a8d09ca3762af61e59520943dc26494f8941b','jmaravilla@gbox.adnu.edu.ph'),
('5db3da50332ff','Sd','Sd','asd','40bd001563085fc35165329ea1ff5c5ecbdbbeef','123@gmail.com'),
('5db44a811aa24','Earl Gabrielle','Basera','blossomblast','e839c2b8c316363ab6c02fecbd120fa4ff6f4216','earlgab9@gmail.com'),
('5db6c39117257','Julius','Daet','jules','3d84ff0760240a74bd3737d8c97b7e0b6c324699','jdaet@gbox.adnu.edu.ph');
/*!40000 ALTER TABLE `USER_PROFILES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WORKSHEETS`
--

DROP TABLE IF EXISTS `WORKSHEETS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WORKSHEETS` (
  `sheet_num` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `instructions` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`sheet_num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WORKSHEETS`
--

LOCK TABLES `WORKSHEETS` WRITE;
/*!40000 ALTER TABLE `WORKSHEETS` DISABLE KEYS */;
INSERT INTO `WORKSHEETS` VALUES (1,'The Basics of Programming and Variables',NULL),(2,'Arithmetic, Relational Operators and Conditional Statements',NULL),(3,'Logical Operators and Iterative Statements',NULL);
/*!40000 ALTER TABLE `WORKSHEETS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-30 11:30:43
