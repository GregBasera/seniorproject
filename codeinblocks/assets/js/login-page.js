$('#passStrength').hide();
$('#status').hide();

function strengthUpdate(){
  var pass = document.Signup.password.value;
  var strength = 0;

  if(pass != ''){
    $('#passStrength').show();
    $('#status').show();
  } else {
    $('#passStrength').hide();
    $('#status').hide();
  }

  if (pass.match(/[A-Z]/)) //Capital
    strength = strength + 25;
  if (pass.match(/[0-9]/)) //Number
    strength = strength + 25;
  if(pass.match(/[~<>?!@#$%^&*()_]/)) //Special Characters
    strength = strength + 25;
  if(pass.length < 10)
    strength = strength + (pass.length*2.5 + 2.5); //length
  if(pass.length >= 10)
    strength = strength + 25; //length

  if(strength < 33.33){
    $('#passProgressBar').removeClass('bg-warning bg-success').addClass('bg-danger');
    document.getElementById('status').innerHTML = "Password Strength: weak"
  }
  else if(strength < 66.66){
    $('#passProgressBar').removeClass('bg-danger bg-success').addClass('bg-warning');
    document.getElementById('status').innerHTML = "Password Strength: moderate"
  }
  else{
    $('#passProgressBar').removeClass('bg-danger bg-warning').addClass('bg-success');
    document.getElementById('status').innerHTML = "Password Strength: strong";
  }

  $('#passProgressBar').attr('style', 'width:'+strength+'%').attr('aria-valuenow', strength);
}

function passwordConfirm(){
  if(document.Signup.confirm.value == document.Signup.password.value){
    $('[name="confirm"]').removeClass('border-danger').addClass('border-success');
  } else {
    $('[name="confirm"]').removeClass('border-success').addClass('border-danger');
  }
}

function enableSubmit_l(){
  if(document.Login.password.value != '' && document.Login.username.value != ''){
    document.Login.submit.disabled = false;
  } else {
    document.Login.submit.disabled = true;
  }
}

function enableSubmit_s(){
  if(document.Signup.password.value != '' && document.Signup.username.value != '' &&
      document.Signup.confirm.value != '' && document.Signup.email.value != '' &&
        document.Signup.fname.value != '' && document.Signup.lname.value != '') {
    document.Signup.submit.disabled = false;
  } else {
    document.Signup.submit.disabled = true;
  }
}

console.log('login-page.js leaded');
