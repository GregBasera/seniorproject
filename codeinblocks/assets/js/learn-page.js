var pythonCode;
var userOutput;
var workspace = Blockly.inject('blocklyDiv', {toolbox: document.getElementById('toolbox'),
  zoom:
       {controls: true,
        wheel: false,
        startScale: 1.0,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2},
   grid:
       {spacing: 20,
        length: 3,
        colour: '#ccc',
        snap: true},
   trashcan: true});
// var defaultBlocks = document.getElementById('blocklyDefault');
var outputArea = document.getElementById('output');
// Blockly.Xml.domToWorkspace(defaultBlocks, workspace);
function myUpdateFunction(event) {
  // var languageDropdown = document.getElementById('languageDropdown');

  var languageSelection = "Python"
  var codeDiv = document.getElementById('codeDiv');
  var codeHolder = document.createElement('pre');
  codeHolder.className = 'prettyprint but-not-that-pretty';
  var code = document.createTextNode(Blockly[languageSelection].workspaceToCode(workspace));
  codeHolder.appendChild(code);
  codeDiv.replaceChild(codeHolder, codeDiv.lastElementChild);
  pythonCode = codeDiv.lastElementChild.innerHTML; //lexical analyzer update.. Greg
  prettyPrint();
}
workspace.addChangeListener(myUpdateFunction);
workspace.addChangeListener(resetInterpreter);


function executeBlockCode() {
  resetInterpreter();
  var code = Blockly.JavaScript.workspaceToCode(workspace); // Its executing blocks on javascript not Python
  var initFunc = function(interpreter, scope) {

    var alertWrapper = function(text) {
      text = text ? text.toString() : '';
      outputArea.value = outputArea.value + text + '\n';
      document.getElementById("output").innerHTML = outputArea;
      // return interpreter.createPrimitive(alert(text));
    };
    interpreter.setProperty(scope, 'alert', interpreter.createNativeFunction(alertWrapper));

    var promptWrapper = function(text) {
      text = text ? text.toString() : '';

      document.getElementById("output").innerHTML = text;
      return interpreter.createPrimitive(prompt(text));
    };

    interpreter.setProperty(scope, 'prompt', interpreter.createNativeFunction(promptWrapper));
  };

  var myInterpreter = new Interpreter(code, initFunc);
  var stepsAllowed = 10000;
  while (myInterpreter.step() && stepsAllowed) {
    stepsAllowed--;
  }
  if (!stepsAllowed) {
    throw EvalError('Infinite loop.');
  }

  lexicalAnalyzer($('.carousel-item.active').attr('key')); // activate lexical analyzer when play button is clicked
}

function resetInterpreter() {
  document.getElementById("output").value = "";
}
resetInterpreter();

document.getElementById('playButton').addEventListener('click', executeBlockCode);

//###################################################################

$('#submitNote').hide();
$('.carousel').carousel({
  // so the slides dont repeat
  wrap: false
});
$("svg.blocklyScrollbarVertical").hide();
$("svg.blocklyScrollbarHorizontal").hide();
pauseSlides(); //theres a bug here, this line prevents it

function slideChange(topic_num, slides_sizeof) {
  // get the number of the previously active slide
  var activeInner = $('.carousel-item.active').html().split(" ");
  var activeName = activeInner[13].split("=");
  var prevSlide = activeName[1].replace('\"', '').replace('"', '');

  setTimeout(function() {
    // get the number of the currently active slide
    var activeInner = $('.carousel-item.active').html().split(" ");
    var activeName = activeInner[13].split("=");
    var activeSlide = activeName[1].replace('\"', '').replace('"', '');

    // construct dataset ready to be relayed to server
    var data = {};
    data['curr_slide'] = parseInt(activeSlide);
    data['topic_num'] = topic_num;
    data['percentage'] = parseInt(activeSlide) / parseInt(slides_sizeof);

    // update css
    $('#progress-bar').attr('style', 'width:' + data['percentage']*100 + '%').attr('aria-valuenow', data['percentage']*100);

    // get proper server and the right function to handle the data
    var base_url = window.location.href;
    var exploded = base_url.split("/");
    var base_url = exploded[2];

    // send to server
    if(activeSlide > parseInt(prevSlide))
      $.ajax({ type: "POST", url: "http://" + base_url + "/learn/slideChange", data: data });

    pauseSlides(); //check if user is currently in a exercise slide.

    if(activeSlide == slides_sizeof){
      $('a.carousel-control-next').attr('href', window.location.origin + "/home");
      $('div.carousel-control-next').html('Back Home').attr('onclick', 'window.location='+"'http://"+base_url+"/home'")
    } else {
      $('a.carousel-control-next').attr('href', "#lecture");
      $('div.carousel-control-next').attr('onclick', "slideChange("+topic_num+", "+slides_sizeof+")")
    }
  }, 100);
}

function pauseSlides(){
  if($('.carousel-item.active').attr('is-exercise') == 1){
    $('a.carousel-control-next').hide();
    $('div.carousel-control-next').hide();
  } else {
    $('a.carousel-control-next').html('').show();
    $('div.carousel-control-next').show();
  }
}

function lexicalAnalyzer(fromServer){
  pythonCode = pythonCode.replace(/\n|\r/g, ''); //remove all line breaks
  userOutput = document.getElementById("output").value.replace(/\n|\r/g, ''); //remove all line breaks

  if(pythonCode != '' && fromServer != ''){
    var answer = fromServer.split("~");
    var key = answer[0].split("|");
    var iscorrect = false;
    var outKey = answer[1].split("|");
    var outMatched = false;

    // console.log(userOutput);
    // console.log(outKey);

    for(var q = 0; q < key.length; q++){
      if(pythonCode.indexOf(key[q]) != -1){
        pythonCode = pythonCode.slice(pythonCode.indexOf(key[q]) + key[q].length);
        iscorrect = true;
        // console.log(pythonCode);
      } else {
        iscorrect = false;
        break;
      }
    }

    for(var q = 0; q < outKey.length; q++){
      if(userOutput.indexOf(outKey[q]) != -1){
        userOutput = userOutput.slice(userOutput.indexOf(outKey[q]) + outKey[q].length);
        outMatched = true;
        console.log(userOutput);
      } else {
        outMatched = false;
        break;
      }
    }
    console.log(outMatched);

    if(iscorrect && outMatched){
      $('a.carousel-control-next').show().html('<i class="fas fa-check-circle fa-5x text-white"></i>');
      $('div.carousel-control-next').show();
    } else {
      $('#lecture').addClass('fa-vibrate2x').attr('style', 'box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6)');
      setTimeout(function(){
        $('#lecture').removeClass('fa-vibrate2x').attr('style', '');
      }, 800);
    }
  }
}

function notesChange(t_num){
  var data = {};
  data['text'] = document.Notes.text.value;
  data['topic_num'] = t_num;

  $.ajax({ type: "POST", url: window.location.origin + "/learn/noteSave", data: data });
  $('#submitNote').hide();
}
