$(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
});
$('.carousel').carousel({
  // so the slides dont repeat
  wrap: false
});

$('#systour').attr('class', 'btn btn-sm btn-primary fa-pulsate mt-2 text-light').html('<i class="fa fa-directions"></i> Website Tour').attr('data-toggle', 'modal').attr('data-target', '.system-tour').click(function(){
  $('#systour').hide();
});

console.log("home-page.js loaded");
