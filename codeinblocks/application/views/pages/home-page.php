<div class="container py-3">
  <div class="row">
    <div class="col-4">
      <!-- User card -->
      <div class="card shadow">
        <div class="card-body">
          <h4>Hello, <?php echo $leftUserCard[0]['fname'] ?></h4>
          <small class="text-muted"><?php echo $leftUserCard[0]['email'] ?></small>
          <hr>
          <h5 class="py-2">Progress Summary</h5>
          <ul class="list-group">
            <?php foreach ($worksheets as $worksheet): ?>
              <li class="list-group-item border-0 px-2 li-hover rounded">
                <div class="row">
                  <div class="col-auto pr-0 d-flex flex-wrap align-content-center cursor-pointer"
                    data-toggle="collapse" data-target="#ws<?php echo $worksheet['sheet_num'] ?>">
                    <i class="fa fa-chalkboard-teacher fa-lg text-black-50" style="color:"></i>
                  </div>
                  <div class="col pr-0 cursor-pointer"
                    data-toggle="collapse" data-target="#ws<?php echo $worksheet['sheet_num'] ?>">
                    <span><b>Session:</b> <?php echo $worksheet['title'] ?></span>
                  </div>
                  <div class="col-3 d-flex flex-wrap justify-content-end align-content-center">
                    <?php
                      $p_sum = 0;
                      $topics = 0;
                      $popover = '';
                      for($q = 0; $q < sizeof($overalls); $q++) {
                        if($overalls[$q]['sheet_num'] == $worksheet['sheet_num']){
                          $p_sum += $overalls[$q]['percentage'];
                          $topics += 1;
                          $popover = $popover.'Topic '.($q+1).': '.ceil($overalls[$q]['percentage']*100).'%<br>';
                        }
                      }
                    ?>
                    <?php if (floor(($p_sum / $topics)*100) != 0): ?>
                      <span class="badge badge-dark cursor-pointer" data-toggle="popover" data-placement="right"
                        data-content="<?php echo '<b>Session '.$worksheet['sheet_num'].':</b><br>'.$popover.'<b>'.ceil(($p_sum / $topics)*100).'% done</b>' ?>" data-html="true">
                        <?php if (ceil(($p_sum / $topics)*100) == 100): ?>
                          <i class="fa fa-check"></i>
                        <?php else: ?>
                          <?php echo ceil(($p_sum / $topics)*100) ?>%
                        <?php endif; ?>
                      </span>
                    <?php endif; ?>
                  </div>
                </div>
              </li>
              <?php if (ceil(($p_sum / $topics)*100) == 100): ?>
              <li class="list-group-item border-0 pr-2 li-hover rounded">
                <div class="row">
                  <div class="col-auto mx-2 cursor-pointer"
                    data-toggle="collapse" data-target="#ws<?php echo $worksheet['sheet_num'] ?>">
                    <i class="fa fa-clipboard-list fa-lg text-black-50"></i>
                  </div>
                  <div class="col p-0 d-flex flex-wrap align-content-center cursor-pointer"
                    data-toggle="collapse" data-target="#ws<?php echo $worksheet['sheet_num'] ?>">
                    <b>Worksheet</b>
                  </div>
                  <div class="col-3 d-flex flex-wrap justify-content-end align-content-center">
                    <span class="badge badge-dark cursor-pointer" data-toggle="popover" data-placement="right"
                      data-content="You got <?php echo '<b>'.$worksheet['score'].'</b> out of <b>'.$worksheet['items'] ?></b> questions in the <b>Worksheet: <?php echo $worksheet['title'] ?></b>." data-html="true">
                      <?php if ($worksheet['score'] == 0): ?>
                        <i class="fa fa-exclamation"></i>
                      <?php else: ?>
                        <?php echo $worksheet['score'].' of '.$worksheet['items'] ?>
                      <?php endif; ?>
                    </span>
                  </div>
                </div>
              </li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
      <!-- End user card -->
    </div>


    <div class="col-8">
      <!-- Banner -->
      <div class="jumbotron py-4 mb-4 shadow">
        <h2 class="mb-2"><?php echo $header[0]['title'] ?></h2>
        <hr>
        <p class="m-0 text-muted"><i><?php echo $header[0]['description'] ?></i></p>
      </div>
      <!-- End Banner -->
      <div class="" id="accordion">
      <?php foreach ($worksheets as $worksheet): ?>
        <!-- Session card -->
        <div class="card mb-3 shadow">
          <div class="card-header rounded cursor-pointer" data-toggle="collapse" data-target="#ws<?php echo $worksheet['sheet_num'] ?>" style="background-color:#e6e6e6">
            <div class="row">
              <div class="col d-flex flex-wrap align-content-center">
                <h5><b><i><?php echo $worksheet['title'] ?></i></b></h5>
              </div>
              <div class="col-auto d-flex flex-wrap align-content-center">
                <?php if ($worksheet['sheet_num'] <= $active_sess && $worksheet['score'] != 0): ?>
                  <i class="text-success fas fa-check-circle fa-flip-diagonal px-3 fa-2x"></i>
                <?php endif; ?>
                <i class="fas fa-caret-down my-auto"></i>
              </div>
            </div>
          </div>
          <div class="collapse <?php if($worksheet['sheet_num'] < $active_sess && $worksheet['score'] == 0) {$active_sess = $worksheet['sheet_num'];}
            if($worksheet['sheet_num'] == $active_sess) {echo 'show';} ?>" id="ws<?php echo $worksheet['sheet_num'] ?>" data-parent="#accordion">
            <div class="card-body">
              <!-- Topic Cards -->
              <?php foreach ($topic_cards as $topic): ?>
                <?php if ($topic['sheet_num'] == $worksheet['sheet_num']): ?>
                  <div class="card border border-secondary mb-4">
                    <?php if ($topic['cover_path'] != null): ?>
                      <img class="card-img-top cover" src="<?php echo base_url().$topic['cover_path'] ?>" alt="">
                    <?php else: ?>
                      <div class="display-3 card-img-top bg-secondary text-light text-center">
                        <?php echo $topic['title'] ?>
                      </div>
                    <?php endif; ?>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-auto">
                          <h3 class="card-title font-weight-bold"><?php echo $topic['title'] ?></h3>
                          <?php if ($topic['curr_slide'] == 0 || $topic['curr_slide'] == 1): ?>
                            <small class="text-muted">You have not started yet.</small>
                          <?php elseif ($topic['percentage'] == 1): ?>
                            <small class="">Finished!</small>
                          <?php else: ?>
                            <small class=""> <?php echo "<i>Currently at slide ".$topic['curr_slide'].". ".ceil($topic['percentage']*100)."% complete.</i>" ?> </small>
                          <?php endif; ?>
                        </div>
                        <div class="col d-flex flex-wrap align-content-center justify-content-end">
                          <?php if ($topic['curr_slide'] == 0): ?>
                            <!-- <a href="<?php echo base_url().'learn/topic/'.$topic['topic_num'] ?>" class="btn btn-primary">Start</a> -->
                            <button class="btn btn-secondary" type="button" data-toggle="tooltip" data-placement="right" title="Please complete previous topics first." disabled>
                              <i class="fas fa-lock"></i>
                            </button>
                          <?php else: ?>
                            <a href="<?php echo base_url().'learn/topic/'.$topic['topic_num'].'/'.$topic['curr_slide'] ?>" class="btn btn-primary">
                              <?php if (($topic['percentage'] *100) == 0): echo "Start" ?>
                              <?php elseif (($topic['percentage'] *100) == 100): echo "Review"?>
                              <?php else: echo "Continue"?>
                              <?php endif; ?>
                            </a>
                          <?php endif; ?>
                        </div>
                      </div>
                      <?php if ($topic['curr_slide'] != 0): ?>
                        <div class="progress" style="height:5px">
                          <div class="progress-bar <?php echo (($topic['percentage'] *100) == 100) ? 'bg-success' : 'bg-secondary' ?>" role="progressbar" style="width:<?php echo ($topic['percentage'] *100).'%' ?>"
                            aria-valuenow="<?php echo ($topic['percentage'] *100) ?>" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>
                  <?php if($topic['sheet_after'] == true && $topic['percentage'] == 1) {$sheet_lock = false;} else {$sheet_lock = true;} ?>
                <?php endif; ?>
              <?php endforeach; ?>
              <!-- Topic Cards End-->

              <!-- Worksheet -->
              <div class="card border border-secondary">
                <div class="card-body">
                  <div class="row">
                    <div class="col-auto d-flex align-items-center">
                      <i class="fas fa-clipboard-list fa-2x"></i>
                    </div>
                    <div class="col pl-0 d-flex align-items-center">
                      <div class="container">
                        <h5 class="m-0"><b>Worksheet: <?php echo $worksheet['title'] ?></b></h5>
                        <?php if ($worksheet['score'] != 0): ?>
                          <small><i>Previous score: </i><?php echo $worksheet['score'].'/'.$worksheet['items']; ?></small>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="col-auto d-flex justify-content-end">
                      <?php if ($sheet_lock == false): ?>
                        <a href="<?php echo base_url().'worksheet/number/'.$worksheet['sheet_ref'] ?>" class="btn btn-success btn-block my-auto">Go</a>
                      <?php else: ?>
                        <button class="btn btn-secondary my-auto" type="button" data-toggle="tooltip" data-placement="right" title="Please complete previous topics first." disabled>
                          <i class="fas fa-lock"></i>
                        </button>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Worksheet End -->
            </div>
          </div>
        </div>
        <!-- Session card End-->
      <?php endforeach; ?>
      </div>
    </div>

  </div>
</div>

<!-- System Tour modal -->
<?php include('application/views/templates/tour.php') ?>
<!-- End Tour modal -->

<div class="container text-muted text-center mb-5">
  <hr>
  <small>© 2019 CodeinBlocks</small>
</div>
