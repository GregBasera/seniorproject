<!-- About the system -->
<div class="container d-flex flex-wrap align-content-center" style="min-height:50vh">
  <div class="row my-4">
    <div class="col-auto">
      <img src="<?php echo base_url().'assets/imgs/favicon3.png' ?>" alt="">
    </div>
    <div class="col d-flex flex-wrap align-content-center">
      <p class="text-monospace m-0">
        CodeInBlocks is a learning platform developed to assist the research entitled: <b>CodeinBlocks: Developing a Visual Learning Environment for Novice Programmers</b>.
        <br>CodeInBlocks integrates <i>Blockly</i>, a Block-based Visual Programming Library, in a <i>Learning Platform</i> with a granular discussion of topics to achieve
        a learning environment hypothesized to be appropriate for novice students. The goal of the study is to find out the effects of using such Visual Programming
        Environment in teaching novice programmers.
      </p>
    </div>
  </div>
</div>
<!-- End system introduction -->

<!-- Proponents -->
<div class="container-fluid mt-5 py-5 bg-secondary text-light" style="min-height:40vh">
  <h2 class="text-center mb-4">Researchers</h2>
  <div class="container">
    <div class="row">
      <div class="col-4">
        <div class="row">
          <div class="col-auto pr-0">
            <i class="fas fa-user-circle fa-5x"></i>
          </div>
          <div class="col">
            <h4 class="mb-2">Greg Emerson Basera</h4>
            <div>Undergraduate</div>
            <div>Ateneo de Naga University</div>
            <div>College of Computer Studies</div>
            <div>4 - BS IT</div>
            <a class="text-info d-block" target="_blank" href="mailto:gbasera@gbox.adnu.edu.ph">gbasera@gbox.adnu.edu.ph</a>
            <a class="text-info d-block" target="_blank" href="mailto:basera.gg@gmail.com">basera.gg@gmail.com</a>
          </div>
        </div>
      </div>
      <div class="col-4 border-left border-right">
        <div class="row">
          <div class="col-auto pr-0">
            <i class="fas fa-user-circle fa-5x"></i>
          </div>
          <div class="col">
            <h4 class="mb-2">Nicohlus Ian Cañeba</h4>
            <div>Undergraduate</div>
            <div>Ateneo de Naga University</div>
            <div>College of Computer Studies</div>
            <div>4 - BS IT</div>
            <a class="text-info d-block" target="_blank" href="mailto:ncaneba@gbox.adnu.edu.ph">ncaneba@gbox.adnu.edu.ph</a>
            <a class="text-info d-block" target="_blank" href="mailto:nicohlusiancaneba@gmail.com">nicohlusiancaneba@gmail.com</a>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="row">
          <div class="col-auto pr-0">
            <i class="fas fa-user-circle fa-5x"></i>
          </div>
          <div class="col">
            <h4 class="mb-2">Janine Mae Llorca</h4>
            <div>Undergraduate</div>
            <div>Ateneo de Naga University</div>
            <div>College of Computer Studies</div>
            <div>4 - BS IT</div>
            <a class="text-info d-block" target="_blank" href="mailto:jmllorca@gbox.adnu.edu.ph">jmllorca@gbox.adnu.edu.ph</a>
            <a class="text-info d-block" target="_blank" href="mailto:janinemaellorca@gmail.com">janinemaellorca@gmail.com</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Proponents -->

<?php include('application/views/templates/tour.php') ?>
