
<div class="container-fluid under-navbar-noscroll">
  <div class="row" style="height:100%">
    <div class="col-6 pr-0">
      <!-- Breadcrumb -->
      <div class="rounded py-1 my-2 px-3" style="background-color:#e6e6e6">
        Lecture: <b><?php echo $topic[0]['title'] ?></b>
      </div>
      <!-- Presentation Area -->
      <?php include('application/views/templates/carousel.php') ?>
      <!-- Arrows -->
      <div class="row bg-dark no-gutters">
        <div class="col-6 carousel-control-prev below" href="#lecture" role="button" data-slide="prev"
         onclick="slideChange(<?php echo $slides[0]['topic_num'] ?>, <?php echo sizeof($slides) ?>)">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </div>
        <div class="col-6 carousel-control-next below" href="#lecture" role="button" data-slide="next"
         onclick="slideChange(<?php echo $slides[0]['topic_num'] ?>, <?php echo sizeof($slides) ?>)">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </div>
      </div>
      <!-- Progress Bar -->
      <div class="progress my-2" style="height:5px">
        <div id="progress-bar" class="progress-bar bg-pimary" role="progressbar" style="width:<?php echo $percentage.'%' ?>"
          aria-valuenow="<?php echo $percentage ?>" aria-valuemin="0" aria-valuemax="100"></div>
      </div>
      <!-- Notes Card -->
      <form name="Notes" target="" action="" method="" style="position:relative">
        <textarea class="form-control" name="text" rows="3" placeholder="Add notes..." onkeyup="($('#submitNote').show())"><?php echo $notes[0]['notes'] ?></textarea>
        <input class="btn btn-primary btn-sm" type="button" id="submitNote" value="Save" style="position:absolute;bottom:5px;right:5px;" onclick="notesChange(<?php echo $slides[0]['topic_num'] ?>)">
      </form>
    </div>


    <div class="col-6 pl-2">
      <div id="blocklyDiv" class="main blockly-panel"></div>
      <div id="codeDiv" class="main output-panel">
        Language: Python
        <hr class="POps mt-1">
        <pre></pre>
      </div>
      <div id="outputDiv" class="main output">
        Output:
        <hr class="POps mt-1">
        <textarea readonly id="output" class="form-control"></textarea>
        <!-- <pre></pre> -->
      </div>
      <!-- <div id="playButton" class="play-button material-icons">play_arrow</div> -->
      <div id="playButton" class="play-button">Run!</div>

      <xml id="toolbox" style="display: none">
        <category id="" colour="" name="Blockly"></category>
        <sep></sep>
        <sep></sep>

        <category id="catLogic" colour="210" name="Logic">
          <block type="controls_if"></block>
          <block type="logic_compare"></block>
          <block type="logic_operation"></block>
          <block type="logic_negate"></block>
          <block type="logic_boolean"></block>
          <block type="logic_null"></block>
          <block type="logic_ternary"></block>
        </category>

        <category id="catLoops" colour="120" name="Loops">
          <block type="controls_repeat_ext">
            <value name="TIMES">
              <shadow type="math_number">
                <field name="NUM">10</field>
              </shadow>
            </value>
          </block>
          <block type="controls_whileUntil"></block>
          <block type="controls_for">
            <value name="FROM">
              <shadow type="math_number">
                <field name="NUM">1</field>
              </shadow>
            </value>
            <value name="TO">
              <shadow type="math_number">
                <field name="NUM">10</field>
              </shadow>
            </value>
            <value name="BY">
              <shadow type="math_number">
                <field name="NUM">1</field>
              </shadow>
            </value>
          </block>
          <block type="controls_forEach"></block>
          <block type="controls_flow_statements"></block>
        </category>

        <category id="catMath" colour="230" name="Math">
          <block type="math_number"></block>
          <block type="math_arithmetic">
            <value name="A">
              <shadow type="math_number">
                <field name="NUM">1</field>
              </shadow>
            </value>
            <value name="B">
              <shadow type="math_number">
                <field name="NUM">1</field>
              </shadow>
            </value>
          </block>
          <block type="math_single">
            <value name="NUM">
              <shadow type="math_number">
                <field name="NUM">9</field>
              </shadow>
            </value>
          </block>
          <block type="math_trig">
            <value name="NUM">
              <shadow type="math_number">
                <field name="NUM">45</field>
              </shadow>
            </value>
          </block>
          <block type="math_constant"></block>
          <block type="math_number_property">
            <value name="NUMBER_TO_CHECK">
              <shadow type="math_number">
                <field name="NUM">0</field>
              </shadow>
            </value>
          </block>
          <block type="math_change">
            <value name="DELTA">
              <shadow type="math_number">
                <field name="NUM">1</field>
              </shadow>
            </value>
          </block>
          <block type="math_round">
            <value name="NUM">
              <shadow type="math_number">
                <field name="NUM">3.1</field>
              </shadow>
            </value>
          </block>
          <block type="math_on_list"></block>
          <block type="math_modulo">
            <value name="DIVIDEND">
              <shadow type="math_number">
                <field name="NUM">64</field>
              </shadow>
            </value>
            <value name="DIVISOR">
              <shadow type="math_number">
                <field name="NUM">10</field>
              </shadow>
            </value>
          </block>
          <block type="math_constrain">
            <value name="VALUE">
              <shadow type="math_number">
                <field name="NUM">50</field>
              </shadow>
            </value>
            <value name="LOW">
              <shadow type="math_number">
                <field name="NUM">1</field>
              </shadow>
            </value>
            <value name="HIGH">
              <shadow type="math_number">
                <field name="NUM">100</field>
              </shadow>
            </value>
          </block>
          <block type="math_random_int">
            <value name="FROM">
              <shadow type="math_number">
                <field name="NUM">1</field>
              </shadow>
            </value>
            <value name="TO">
              <shadow type="math_number">
                <field name="NUM">100</field>
              </shadow>
            </value>
          </block>
          <block type="math_random_float"></block>
        </category>

        <category id="catText" colour="160" name="Text">
          <block type="text"></block>
          <block type="text_print">
            <value name="TEXT">
              <shadow type="text">
                <field name="TEXT">abc</field>
              </shadow>
            </value>
          </block>
          <block type="text_join"></block>
          <block type="text_append">
            <value name="TEXT">
              <shadow type="text"></shadow>
            </value>
          </block>
          <block type="text_length">
            <value name="VALUE">
              <shadow type="text">
                <field name="TEXT">abc</field>
              </shadow>
            </value>
          </block>
          <block type="text_isEmpty">
            <value name="VALUE">
              <shadow type="text">
                <field name="TEXT"></field>
              </shadow>
            </value>
          </block>
          <block type="text_indexOf">
            <value name="VALUE">
              <block type="variables_get">
                <field name="VAR">text</field>
              </block>
            </value>
            <value name="FIND">
              <shadow type="text">
                <field name="TEXT">abc</field>
              </shadow>
            </value>
          </block>
          <block type="text_charAt">
            <value name="VALUE">
              <block type="variables_get">
                <field name="VAR">text</field>
              </block>
            </value>
          </block>
          <block type="text_getSubstring">
            <value name="STRING">
              <block type="variables_get">
                <field name="VAR">text</field>
              </block>
            </value>
          </block>
          <block type="text_changeCase">
            <value name="TEXT">
              <shadow type="text">
                <field name="TEXT">abc</field>
              </shadow>
            </value>
          </block>
          <block type="text_trim">
            <value name="TEXT">
              <shadow type="text">
                <field name="TEXT">abc</field>
              </shadow>
            </value>
          </block>
          <block type="text_prompt_ext">
            <value name="TEXT">
              <shadow type="text">
                <field name="TEXT">abc</field>
              </shadow>
            </value>
          </block>
        </category>

        <category id="catLists" colour="260" name="Lists">
          <block type="lists_create_with">
            <mutation items="0"></mutation>
          </block>
          <block type="lists_create_with"></block>
          <block type="lists_repeat">
            <value name="NUM">
              <shadow type="math_number">
                <field name="NUM">5</field>
              </shadow>
            </value>
          </block>
          <block type="lists_length"></block>
          <block type="lists_isEmpty"></block>
          <block type="lists_indexOf">
            <value name="VALUE">
              <block type="variables_get">
                <field name="VAR">list</field>
              </block>
            </value>
          </block>
          <block type="lists_getIndex">
            <value name="VALUE">
              <block type="variables_get">
                <field name="VAR">list</field>
              </block>
            </value>
          </block>
          <block type="lists_setIndex">
            <value name="LIST">
              <block type="variables_get">
                <field name="VAR">list</field>
              </block>
            </value>
          </block>
          <block type="lists_getSublist">
            <value name="LIST">
              <block type="variables_get">
                <field name="VAR">list</field>
              </block>
            </value>
          </block>
          <block type="lists_split">
            <value name="DELIM">
              <shadow type="text">
                <field name="TEXT">,</field>
              </shadow>
            </value>
          </block>
          <block type="lists_sort"></block>
        </category>

        <category id="catColour" colour="20" name="Color">
          <block type="colour_picker"></block>
          <block type="colour_random"></block>
          <block type="colour_rgb">
            <value name="RED">
              <shadow type="math_number">
                <field name="NUM">100</field>
              </shadow>
            </value>
            <value name="GREEN">
              <shadow type="math_number">
                <field name="NUM">50</field>
              </shadow>
            </value>
            <value name="BLUE">
              <shadow type="math_number">
                <field name="NUM">0</field>
              </shadow>
            </value>
          </block>
          <block type="colour_blend">
            <value name="COLOUR1">
              <shadow type="colour_picker">
                <field name="COLOUR">#ff0000</field>
              </shadow>
            </value>
            <value name="COLOUR2">
              <shadow type="colour_picker">
                <field name="COLOUR">#3333ff</field>
              </shadow>
            </value>
            <value name="RATIO">
              <shadow type="math_number">
                <field name="NUM">0.5</field>
              </shadow>
            </value>
          </block>
        </category>

        <sep></sep>
        <category id="catVariables" colour="330" custom="VARIABLE" name="Variables"></category>
        <category id="catFunctions" colour="290" custom="PROCEDURE" name="Functions"></category>
      </xml>
    </div>
  </div>
</div>

<?php include('application/views/templates/tour.php') ?>
