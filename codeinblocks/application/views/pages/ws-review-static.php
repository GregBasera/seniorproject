<div class="container">
  <div class="row d-flex justify-content-between">
    <div class="col-auto">
      <h3 class="my-3">Worksheet Review</h3>
    </div>
    <div class="col-auto">
      <h3 class="my-3">Score: <span class="badge badge-secondary"><?php echo $score." out of ".sizeof($questions) ?></span></h3>
    </div>
    <div class="col-auto">
      <a class="my-3 btn btn-primary" href="<?php echo base_url().'home' ?>">Return Home</a>
    </div>
  </div>

  <table class="table table-bordered">
    <thead class="thead-light">
      <tr class="text-center">
        <th>Question</th>
        <th colspan="3">Choices</th>
        <th>Correct Answer</th>
        <th>Your Answer</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($answers as $name => $val): ?>
        <?php foreach ($questions as $question): ?>
          <?php if ($name == ('q'.$question['question_ID'])): ?>
            <tr class="<?php if($val == $question['Answer']) echo "table-success"; else if (strlen($val) > 1) echo "table-info"; else echo "table-danger"?>">
              <td><?php echo $question['question'] ?></td>
              <td data-toggle="tooltip" data-placement="bottom" title="<?php echo $question['option_A'] ?>"><span class="badge badge-pill badge-secondary">A</span></td>
              <td data-toggle="tooltip" data-placement="bottom" title="<?php echo $question['option_B'] ?>"><span class="badge badge-pill badge-secondary">B</span></td>
              <td data-toggle="tooltip" data-placement="bottom" title="<?php echo $question['option_C'] ?>"><span class="badge badge-pill badge-secondary">C</span></td>
              <td class="text-center"><?php echo $question['Answer'] ?></td>
              <td class="text-center"><?php echo $val ?></td>
            </tr>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>

<?php include('application/views/templates/tour.php') ?>
