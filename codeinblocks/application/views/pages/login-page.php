<div class="container-fluid" style="background-image:url(<?php echo base_url().'assets/imgs/Login-BG.png' ?>);background-repeat:no-repeat;background-size:cover;">
  <div class="container d-flex flex-wrap align-content-center justify-content-center" style="height:100vh;">

    <div class="card shadow w-50">
      <img class="card-img-top" src="<?php echo base_url();?>assets/imgs/logo.png"  alt="Logo">
      <div class="card-body">
        <h5 class="card-title">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link <?php echo ($form_type == 'l') ? 'active' : ''; ?>" href="<?php echo base_url().'log/index/l'; ?>">Log In</a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php echo ($form_type == 's') ? 'active' : ''; ?>" href="<?php echo base_url().'log/index/s'; ?>">Sign Up</a>
            </li>
          </ul>
        </h5>
        <?php if ($form_type == 'l'): ?>
          <!-- Login Form -->
          <form name="Login" class="" target="" action="<?php echo base_url(); ?>log/in" name="" method="post">
            <!-- Username -->
            <div class="form-group">
              <label for="username"><small>Username</small></label>
              <input type="text" class="form-control" id="username" name="username" onkeyup="enableSubmit_l()" required>
            </div>
            <!-- Password -->
            <div class="form-group">
              <label for="password"><small>Password</small></label>
              <input type="password" class="form-control" id="password" name="password" onkeyup="enableSubmit_l()" required>
            </div>

            <!-- Alerts -->
            <div class="alert alert-danger" <?php echo ($error == 1) ? '' : 'hidden' ?>>
              Sorry, we didn't recognize the credentials you've entered. Please try again or Sign up.
            </div>

            <input class="btn btn-primary btn-sm btn-block" type="submit" name="submit" value="Login" disabled>
          </form>
        <?php elseif ($form_type == 's'): ?>
          <!-- Signup Form -->
          <form name="Signup" class="" target="" action="<?php echo base_url(); ?>log/signup" method="post">
            <!-- Fname and Lname -->
            <div class="form-group mb-1 row">
              <div class="col pr-1">
                <label for="fname"><small>Firstname</small></label>
                <input type="text" class="form-control form-control-sm" name="fname" onkeyup="enableSubmit_s()" required>
              </div>
              <div class="col pl-1">
                <label for="lname"><small>Lastname</small></label>
                <input type="text" class="form-control form-control-sm" name="lname" onkeyup="enableSubmit_s()" required>
              </div>
            </div>
            <!-- Username -->
            <div class="form-group mb-1">
              <label for="username"><small>Username</small></label>
              <input type="text" class="form-control form-control-sm" name="username" onkeyup="enableSubmit_s()" required>
            </div>
            <!-- Email -->
            <div class="form-group mb-1">
              <label for="email"><small>E-mail</small></label>
              <input type="email" class="form-control form-control-sm" name="email" value="" onkeyup="enableSubmit_s()" required>
            </div>
            <!-- Password -->
            <div class="form-group mb-1">
              <label for="password"><small>Password</small></label>
              <input type="password" class="form-control form-control-sm" name="password" onkeyup="strengthUpdate();passwordConfirm();enableSubmit_s();" required>
              <small><i id="status">Password Strength: </i></small>
              <div id="passStrength" class="progress mt-1" style="height:5px;">
                <div id="passProgressBar" class="progress-bar bg-danger" style="width:0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
            <!-- Confirm password -->
            <div class="form-group">
              <label for="confirm"><small>Confirm Password</small></label>
              <input type="password" class="form-control form-control-sm" name="confirm" onkeyup="passwordConfirm();enableSubmit_s();" required>
            </div>

            <!-- Alerts -->
            <div class="alert alert-danger" role="alert" hidden>
              We've detected an invalid input. Please try again.
            </div>

            <input class="btn btn-primary btn-sm btn-block" type="submit" name="submit" value="Signup" disabled>
          </form>
        <?php endif; ?>
      </div>
    </div>

  </div>
</div>
