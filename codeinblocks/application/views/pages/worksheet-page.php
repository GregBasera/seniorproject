<div class="container-fluid">
  <div class="row mt-3">
    <div class="col-3"><!-- spacer --></div>


    <div class="col-6">
      <h3>Worksheet: <?php echo $worksheet[0]['title']; ?></h3>
      <hr>
      <p><b>Instructions:</b>Choose the best solution for the questions below.</p>

      <!-- Questionnaire form -->
      <form class="mt-3" target="" action="<?php echo base_url(). 'worksheet/checking/' .$sheet_num ?>" name="" method="post">
      <?php $question_num = 1 ?>
      <?php foreach ($questions as $question): ?>
        <!-- Question -->
        <div class="jumbotron jumbotron-fluid py-3">
          <div class="container">
            <div class="row">
              <div class="col-auto border-right">
                <span class="badge badge-secondary"><?php echo $question_num ?></span>
              </div>
              <div class="col">
                <div class="mb-3" style="font-size:14pt;">
                  <?php echo $question['question'] ?>
                </div>

                <!-- Options -->
                <div class="btn-group-toggle btn-group-vertical btn-block px-5" data-toggle="buttons">
                  <?php if ($question['option_A'] == null && $question['option_B'] == null && $question['option_C'] == null): ?>
                    <textarea class="form-control" rows="3" name="q<?php echo $question['question_ID'] ?>" required></textarea>
                  <?php else: ?>
                    <?php if ($question['option_A'] != null): ?>
                      <label class="btn btn-outline-success text-dark rounded my-1">
                        <input type="radio" name="q<?php echo $question['question_ID'] ?>" value="A" required>
                        <?php echo $question['option_A'] ?>
                      </label>
                    <?php endif; ?>
                    <?php if ($question['option_B'] != null): ?>
                      <label class="btn btn-outline-success text-dark rounded my-1">
                        <input type="radio" name="q<?php echo $question['question_ID'] ?>" value="B" required>
                        <?php echo $question['option_B'] ?>
                      </label>
                    <?php endif; ?>
                    <?php if ($question['option_C'] != null): ?>
                      <label class="btn btn-outline-success text-dark rounded my-1">
                        <input type="radio" name="q<?php echo $question['question_ID'] ?>" value="C" required>
                        <?php echo $question['option_C'] ?>
                      </label>
                    <?php endif; ?>
                  <?php endif; ?>
                </div>
                <!-- End options -->
              </div>
            </div>
          </div>
        </div>
        <?php $question_num++ ?>
        <!-- End question -->
      <?php endforeach; ?>

      <button type="button" class="btn btn-outline-success btn-block mb-4" data-toggle="modal" data-target="#Confirm">
        Submit
      </button>

        <div class="modal" id="Confirm" tabindex="-1" role="dialog" aria-labelledby="ConfirmBox" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="ConfirmBox">Confirm</h5>
              </div>
              <div class="modal-body">
                Submit your answers?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <input class="btn btn-primary" type="submit" name="done" value="Submit">
              </div>
            </div>
          </div>
        </div>
      </form>
      <!-- End questionnaire form -->

    </div>


    <div class="col-3"><!-- spacer --></div>
  </div>
</div>

<?php include('application/views/templates/tour.php') ?>
