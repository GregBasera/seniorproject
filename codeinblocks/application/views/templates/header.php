<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CodeinBlocks -- <?php echo $title; ?></title>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()."assets/imgs/favicon4.png" ?>">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/general.css">

    <?php if ($title == 'Learn' || $title == 'Sandbox'): ?>
      <link href="<?php echo base_url();?>assets/css/lecture.css" rel="stylesheet">
      <link href="<?php echo base_url();?>blockly/blockly-demo.css" rel="stylesheet">
    <?php elseif ($title == 'Worksheet'): ?>
      <link href="<?php echo base_url();?>assets/css/worksheet.css" rel="stylesheet">
    <?php endif; ?>
  </head>
  <body>
    <!-- Screen too small alert -->
    <div class="alert alert-danger text-center too-small">
      The elements used in this webpage is best viewed in a Desktop environment.
    </div>
    <!-- End Alert -->
  <?php if ($title != 'Login'): ?>
    <!-- Navbar -->
    <nav id="navbar" class="navbar sticky-top navbar-expand-lg bg-light shadow-sm">
      <!-- CodeinBlocks logo -->
      <a class="navbar-brand" href="<?php echo base_url().'home' ?>">
        <img src="<?php echo base_url();?>assets/imgs/logo.png" style="height:30px;" />
      </a>
      <!-- End logo -->

      <!-- Nav pages -->
      <ul class="nav mr-auto">
        <li class="nav-item">
          <a class="nav-link text-dark <?php echo ($title == 'Home') ? 'active' : '' ?>" href="<?php echo base_url().'home' ?>">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark <?php echo ($title == 'Sandbox') ? 'active' : '' ?>" href="<?php echo base_url().'sandbox' ?>">Sandbox</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark <?php echo ($title == 'About') ? 'active' : '' ?>" href="<?php echo base_url().'about' ?>">About</a>
        </li>
      </ul>
      <!-- End nav pages -->

      <!-- Current User options -->
      <div class="btn-group">
        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-lg"></i>
          <?php echo $username ?>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
          <button class="dropdown-item btn" data-toggle="modal" data-target=".system-tour">
            <i class="fas fa-directions mr-2"></i>
            Website Tour
          </button>
          <a href="<?php echo base_url(); ?>log/out" class="dropdown-item btn" type="button">
            <i class="fas fa-sign-out-alt mr-2"></i>
            Log out
          </a>
        </div>
      </div>
      <!-- End current user options -->
    </nav>
    <!-- End Navbar -->
  <?php endif; ?>
