  <!-- <div class="container-fluid" style="height:15vh">
  </div> -->
  </body>

  <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <?php if ($title == 'Learn' || $title == 'Sandbox'): ?>
    <script src="<?php echo base_url();?>blockly/blockly_compressed.js"></script>
    <script src="<?php echo base_url();?>blockly/blocks_compressed.js"></script>
    <script src="<?php echo base_url();?>blockly/javascript_compressed.js"></script>
    <script src="<?php echo base_url();?>blockly/python_compressed.js"></script>
    <script src="<?php echo base_url();?>blockly/acorn_interpreter.js"></script>
    <script src="<?php echo base_url();?>blockly/prettify-bundle.js"></script>
    <script src="<?php echo base_url();?>blockly/en.js"></script>
    <!-- <script src="<?php echo base_url();?>wait_block.js"></script> -->
    <script src="<?php echo base_url();?>assets/js/learn-page.js"></script>
  <?php elseif ($title == 'Home'): ?>
    <script src="<?php echo base_url();?>assets/js/home-page.js"></script>
  <?php elseif ($title == 'Login'): ?>
    <script src="<?php echo base_url();?>assets/js/login-page.js"></script>
  <?php elseif ($title == 'Worksheet'): ?>
    <script src="<?php echo base_url();?>assets/js/worksheet.js"></script>
  <?php endif; ?>

  <script type="text/javascript">
    $(function(){
      // this is to instruct the browser would load
      // the tooltip and popover scripts from bootstrap
      $('[data-toggle="tooltip"]').tooltip();
      $('[data-toggle="popover"]').popover();
    });
  </script>
</html>
