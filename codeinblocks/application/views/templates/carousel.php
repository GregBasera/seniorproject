<div id="lecture" class="carousel border border-secondary" data-ride="carousel" data-interval="false" data-keyboard="false">
  <div class="carousel-inner" role="listbox">
    <?php foreach ($slides as $slide): ?>
      <div class="carousel-item <?php if($slide['slide_num'] == $curr_slide) echo 'active'?>" is-exercise="<?php echo $slide['is_practice_slide'] ?>" key="<?php echo $slide['practice_key'] ?>">
        <img class="d-block w-100" src="<?php echo base_url().$slide['file_path'] ?>" data-src="" name="<?php echo $slide['slide_num'] ?>" alt="Lecture slide">
      </div>
    <?php endforeach; ?>
  </div>
  <a class="carousel-control-prev" href="#lecture" role="button" data-slide="prev" onclick="slideChange(<?php echo $slides[0]['topic_num'] ?>, <?php echo sizeof($slides) ?>)">
  </a>
  <a class="carousel-control-next" href="#lecture" role="button" data-slide="next" onclick="slideChange(<?php echo $slides[0]['topic_num'] ?>, <?php echo sizeof($slides) ?>)">
  </a>
</div>
