<div class="modal fade system-tour" tabindex="-1" role="dialog" aria-labelledby="myHugeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content">

      <div class="bd-example">
        <div id="systemTour" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
          <ol class="carousel-indicators">
            <li data-target="#systemTour" data-slide-to="0" class="active"></li>
            <li data-target="#systemTour" data-slide-to="1"></li>
            <li data-target="#systemTour" data-slide-to="2"></li>
            <li data-target="#systemTour" data-slide-to="3"></li>
            <li data-target="#systemTour" data-slide-to="4"></li>
            <li data-target="#systemTour" data-slide-to="5"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <img class="d-block w-100" src="<?php echo base_url().'assets/imgs/systour/Tour_1.png' ?>" alt="1st slide">
              <div class="carousel-caption d-none d-md-block">
                <small>1/6</small>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="<?php echo base_url().'assets/imgs/systour/Tour_2.png' ?>" alt="2nd slide">
              <div class="carousel-caption d-none d-md-block">
                <small>2/6</small>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="<?php echo base_url().'assets/imgs/systour/Tour_3.png' ?>" alt="3rd slide">
              <div class="carousel-caption d-none d-md-block">
                <small>3/6</small>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="<?php echo base_url().'assets/imgs/systour/Tour_4.png' ?>" alt="4th slide">
              <div class="carousel-caption d-none d-md-block">
                <small>4/6</small>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="<?php echo base_url().'assets/imgs/systour/Tour_5.png' ?>" alt="4th slide">
              <div class="carousel-caption d-none d-md-block">
                <small>5/6</small>
              </div>
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="<?php echo base_url().'assets/imgs/systour/Tour_6.png' ?>" alt="4th slide">
              <div class="carousel-caption d-none d-md-block">
                <small>6/6</small>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#systemTour" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#systemTour" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

    </div>
  </div>
</div>
