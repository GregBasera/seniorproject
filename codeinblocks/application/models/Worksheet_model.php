<?php
class Worksheet_model extends CI_Model{
  // this function queries the database for the title of a specified
  // worksheet
  public function getWorksheetTitle($num) {
    $query = $this->db->query("select title from WORKSHEETS where sheet_num = '$num';");
    $result = $query->result_array();
    return $result;
  }

  // This function pulls all the questions attributed to a specified
  // worksheet number
  public function getByQuestions($num) {
    $query = $this->db->query("select * from QUESTIONS where sheet_num = $num ORDER BY RAND();");
    $result = $query->result_array();
    return $result;
  }

  // when the user finishes a worksheet, his score is generated and is
  // saved in the database. the task is done in this function. at the
  // same time an activity saying the users completion is inserted to
  // the database
  public function updateScore($score, $id, $sheet_num) {
    $this->db->set('score', $score);
    $this->db->where('sheet_num', $sheet_num);
    $this->db->where('user_ID', $id);
    $this->db->update('SESSIONS');

    $data = array(
      'user_ID' => $id,
      'activity_ID' => 'cibw000'.$sheet_num
    );
    $this->db->insert('ACTIVITY_LOGS', $data);
  }

  public function refToNum($ref) {
    $query = $this->db->query("
      select sheet_num
      from WORKSHEETS where sheet_ref = '$ref';
    ");
    $result = $query->result_array();
    return $result;
  }
}
