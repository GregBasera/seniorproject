<?php
class Home_model extends CI_Model{
  // this function gathers data from the database intended for the jumbotron
  // in the homepage. it first organizes the results from the most recent and
  // then just returns the very first row
  public function getRecentActivity($id){
    $query = $this->db->query("
      select b.date_time, b.user_ID, b.activity_ID, a.title, a.description
      from ACTIVITIES a, ACTIVITY_LOGS b
      where a.activity_ID = b.activity_ID
      and b.user_ID = '$id'
      order by b.date_time desc limit 1;
    ");
    $result = $query->result_array();
    return $result;
  }

  // this function pulls the necessary data from the database intended for
  // the construction of the sessions in the homepage.
  public function joinTopicAndProgress($id) {
    $query = $this->db->query("
      select t.topic_num, t.title, t.description, t.cover_path, t.sheet_after, t.sheet_num, p.user_ID, p.curr_slide, p.percentage
      from TOPICS t, PROGRESS p
      where t.topic_num = p.topic_num
      and p.user_ID = '$id';
    ");
    $result = $query->result_array();
    return $result;
  }

  // this function pulls the necessary data from the database intended for
  // the construction of the sessions in the homepage.
  public function getAllWorksheets($id) {
    $query = $this->db->query("
      select w.sheet_ref, w.sheet_num, w.title, w.instructions, s.user_ID, s.score, (select count(*) from QUESTIONS where sheet_num = s.sheet_num) as items
      from WORKSHEETS w, SESSIONS s
      where s.user_ID = '$id'
      and w.sheet_num = s.sheet_num;
    ");
    $result = $query->result_array();
    return $result;
  }

  // this function returns, through some sql joins, the current topic the user
  // is in.
  public function activeSession() {
    $query = $this->db->query("
      select sheet_num
      from TOPICS
      where topic_num = (select topic_num
                          from PROGRESS
                          where curr_slide != 0
                          order by topic_num desc
                          limit 1);
    ");
    $result = $query->result_array();
    return $result;
  }

  // this function quatifies users overall progress on a session. this is needed
  // to construct the user card in the home page
  public function overallProgress($id) {
    $query = $this->db->query("
      select t.sheet_num, p.percentage
      from TOPICS t, PROGRESS p
      where t.topic_num = p.topic_num
      and p.user_ID = '$id';
    ");
    $result = $query->result_array();
    return $result;
  }

  // this function returns the users email and username. these is used to construct
  // the user card in the homepage
  public function leftUserCard($id){
    $query = $this->db->query("select fname, lname, username, email from USER_PROFILES where user_ID = '$id'");
    $result = $query->result_array();
    return $result;
  }
}
