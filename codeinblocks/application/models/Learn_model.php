<?php
class Learn_model extends CI_Model{
  // this function pulls all the slides attributed to a topic from the
  // database so it can be constrcted in the carousel
  public function getAllById($id){
    $query = $this->db->query("select * from SLIDES where topic_num = $id;");
    $result = $query->result_array();
    return $result;
  }

  // this function queries the database for the notes the user save for
  // a specific topic
  public function getTopicNotes($user_ID, $topic_num){
    $query = $this->db->query("select notes from PROGRESS where user_ID = '$user_ID' and topic_num = '$topic_num';");
    $result = $query->result_array();
    return $result;
  }

  // this function queries the database for details about the topic
  public function getTopicDetails($topic_num){
    $query = $this->db->query("select * from TOPICS where topic_num = '$topic_num';");
    $result = $query->result_array();
    return $result;
  }

  // when the user clicks the next button on the carousel his current slide
  // attribute in the database must be updated, that task is done here. if
  // dont have any progress yet, this function will initialize him one, else
  // his previous progress will be updated
  public function currSlideChange($user_ID, $topic_num, $curr_slide, $percentage){
    $query = $this->db->query("select * from PROGRESS where user_ID = '$user_ID' and topic_num = '$topic_num';");
    // mysqli_next_result($this->db->conn_id); // idk what this does.
    $result = $query->result_array();

    if(empty($result)){
      $data = array(
        'user_ID' => $user_ID,
        'topic_num' => $topic_num,
        'curr_slide' => $curr_slide,
        'percentage' => $percentage
      );

      $this->db->insert('PROGRESS', $data);
    } else {
      if($result[0]['curr_slide'] < $curr_slide) {
        $this->db->set('curr_slide', $curr_slide);
        $this->db->set('percentage', $percentage);
        $this->db->where('user_ID', $user_ID);
        $this->db->where('topic_num', $topic_num);
        $this->db->update('PROGRESS');
      }
    }
  }

  // when the user is done with a topic this function is called. this function
  // initializes the next topic in the databse for the user. at the same time
  // it also logs this event in the users activity log
  public function initNextCurrDone($user_ID, $topic_num, $curr_slide, $activity_ID){
    $this->db->set('curr_slide', $curr_slide);
    $this->db->where('user_ID', $user_ID);
    $this->db->where('topic_num', $topic_num);
    $this->db->update('PROGRESS');

    $act = array(
      'user_ID' => $user_ID,
      'activity_ID' => $activity_ID
    );
    $this->db->insert('ACTIVITY_LOGS', $act);
  }

  // when the user edits his notes in the topic, it gets saved in the database as well
  // this function does that
  public function updateNotes($user_ID, $topic_num, $text){
    $this->db->set('notes', $text);
    $this->db->where('user_ID', $user_ID);
    $this->db->where('topic_num', $topic_num);
    $this->db->update('PROGRESS');
  }
}
