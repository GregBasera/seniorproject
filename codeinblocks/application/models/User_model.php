<?php
class User_model extends CI_Model{
  // this functions gets a users ID given his username and password
  // if the this function returns a empty object the user is not
  // permitted to login else he is.
  public function login($user, $pass){
    $query = $this->db->query("
      select user_ID
      from USER_PROFILES
      where username = '$user'
      and password = '$pass';
    ");
    $result = $query->result_array();
    return $result;
  }

  // this funtion returns true if a user is successfuly inserted to the
  // database. else it returns false. after the users credentials has been
  // inserted, his progress ans activity logs are initialized
  public function signup($creds, $activity){
    try {
      $this->db->insert('USER_PROFILES', $creds);

      $query = $this->db->query("select topic_num from TOPICS");
      for($q = 1; $q <= $query->num_rows(); $q++){
        $data = array(
          'user_ID' => $creds['user_ID'],
          'topic_num' => $q,
          'curr_slide' => ($q == 1) ? 1 : 0,
          'percentage' => 0.0
        );

        $this->db->insert('PROGRESS', $data);
      }

      $query = $this->db->query("select sheet_num from WORKSHEETS");
      for($q = 1; $q <= $query->num_rows(); $q++){
        $data = array(
          'sheet_num' => $q,
          'user_ID' => $creds['user_ID'],
          'score' => 0
        );

        $this->db->insert('SESSIONS', $data);
      }

      $this->db->insert('ACTIVITY_LOGS', $activity);
      return true;
    } catch (Exception $e) {
      return false;
    }
  }
}
