<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Learn extends CI_Controller {
	public function __construct() {
    parent::__construct();
		if(empty($_SESSION['u_id']) || !isset($_SESSION['u_id'])) {
			redirect('log', 'refresh');
		}
  }

	// When a user chooses to start or review a topic, the request is processed here.
	// data is first gathered from the database through a model and then sent to the
	// client as a html page.
	public function topic($id = 1, $curr_slide = 1) {
    $page['title'] = "Learn";
		$page['username'] = $_SESSION['user'];

		$data['slides'] = $this->learn_model->getAllById($id);
		$data['notes'] = $this->learn_model->getTopicNotes($_SESSION['u_id'], $id);
		$data['topic'] = $this->learn_model->getTopicDetails($id);
		// if the curr_slide from the database is 0 or if the user havent started yet
		// the curr_slide is defaultly set to 1 as the first slide. And then the percentage
		// is calculated using the number of slide and curr_slide.
		$data['curr_slide'] = ($curr_slide == "0") ? "1" : $curr_slide;
		$data['percentage'] = ($data['curr_slide'] / sizeof($data['slides'])) *100;

    $this->load->view('templates/header', $page);
    $this->load->view('pages/learn-page', $data);
    $this->load->view('templates/footer');
	}

	// When a user clicked the next button on a slide he is actually sending a request to
	// the server to change his current slide attribute in the progress table. the request
	// is processed here. the request is directed to a model.
	public function slideChange() {
		$topic_num = $this->input->post('topic_num');
		$curr_slide = $this->input->post('curr_slide');
		$percentage = $this->input->post('percentage');

		$this->learn_model->currSlideChange($_SESSION['u_id'], $topic_num, $curr_slide, $percentage);

		if($percentage == 1){
			$this->learn_model->initNextCurrDone($_SESSION['u_id'], intval($topic_num)+1, 1, 'cib0000'.$topic_num);
		}
	}

	// When a user saves a note his request is sent here. the string us preprocessed and is
	// sent to a model ready to be inserted in the database.
	public function noteSave(){
		$text = $this->input->post('text');
		$topic = $this->input->post('topic_num');

		$this->learn_model->updateNotes($_SESSION['u_id'], $topic, $text);
	}
}
