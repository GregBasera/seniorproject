<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worksheet extends CI_Controller {
	public function __construct() {
    parent::__construct();
		if(empty($_SESSION['u_id']) || !isset($_SESSION['u_id'])) {
			redirect('log', 'refresh');
		}
  }

	// this function loads the worksheetpage where the user can answer the worksheet
  public function number($sheet_ref){
    $page['title'] = "Worksheet";
		$page['username'] = $_SESSION['user'];

		$sheet_num = $this->worksheet_model->refToNum($sheet_ref);
		$sheet_num = $sheet_num[0]['sheet_num'];
		$data['worksheet'] = $this->worksheet_model->getWorksheetTitle($sheet_num);
		$data['questions'] = $this->worksheet_model->getByQuestions($sheet_num);
		$data['sheet_num'] = $sheet_num;

    $this->load->view('templates/header', $page);
    $this->load->view('pages/worksheet-page', $data);
    $this->load->view('templates/footer');
  }

	// when the user is done with the worksheet his answers is sent to this function
	// it is then checked here. users score is updated and a page with all the questions
	// and answers is shown to the client for review.
	public function checking($sheet_num) {
		$keys = $this->worksheet_model->getByQuestions($sheet_num);
		$answers = $this->input->post();

		$score = 0;
		// with each user answer
		foreach ($answers as $name => $val) {
			// find the correnponding key from the database
			foreach ($keys as $key) {
				if($name == ('q'.$key['question_ID'])){
					// the key in the database is in tokens because there are questions
					// that requires user to write code
					$tokens = explode("|", $key['Answer']);
					$correct = false;
					// loop through the tokens and the characters of the users answer
					for ($q=0; $q < sizeof($tokens); $q++) {
						// if a token matches
						if(strpos($val, $tokens[$q]) !== false){
							// set a variable to true
							$correct = true;
							// cut the string where the match is
							$val = substr($val, strpos($val, $correct[$q])+strlen($correct[$q]));
						} else {
							// else set the variable to false and break the loop
							$correct = false;
							break;
							// the string match the tokens, the loop will end with the variable being true
						}
					}
					// if the loop ended with a true variable the score is incremented
					if($correct) $score++;
				}
			}
		}

		$this->worksheet_model->updateScore($score, $_SESSION['u_id'], $sheet_num);

		$page['title'] = "Worksheet";
		$page['username'] = $_SESSION['user'];

		$data['questions'] = $this->worksheet_model->getByQuestions($sheet_num);
		$data['answers'] = $answers;
		$data['score'] = $score;

		$this->load->view('templates/header', $page);
		$this->load->view('pages/ws-review-static', $data);
		$this->load->view('templates/footer');
	}
}
