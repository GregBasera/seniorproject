<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	public function __construct() {
    parent::__construct();
		if(empty($_SESSION['u_id']) || !isset($_SESSION['u_id'])) {
			redirect('log', 'refresh');
		}
  }

	public function index() {
    $page['title'] = "About";
		$page['username'] = $_SESSION['user'];

		// this is a static page that displays information about the system
    $this->load->view('templates/header', $page);
    $this->load->view('pages/about-page');
    $this->load->view('templates/footer');
	}
}
