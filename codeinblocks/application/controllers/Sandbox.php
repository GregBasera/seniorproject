<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sandbox extends CI_Controller {
	public function __construct() {
    parent::__construct();
		if(empty($_SESSION['u_id']) || !isset($_SESSION['u_id'])) {
			redirect('log', 'refresh');
		}
  }

	public function index() {
    $page['title'] = "Sandbox";
		$page['username'] = $_SESSION['user'];

		// This is a static page that showcases Blockly and
		// allows users to play around with it.
    $this->load->view('templates/header', $page);
    $this->load->view('pages/sandbox-page');
    $this->load->view('templates/footer');
	}
}
