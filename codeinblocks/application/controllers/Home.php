<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	// this function checks if a session is saved for a user
	// if a session is saved he is allowed to access the index function
	// if not the user is redirected to loginpage. This is a fuction seen
	// in most controller class
	public function __construct() {
    parent::__construct();
		if(empty($_SESSION['u_id']) || !isset($_SESSION['u_id'])) {
			redirect('log', 'refresh');
		}
  }

	public function index() {
    $page['title'] = "Home";
		$page['username'] = $_SESSION['user'];

		// The left navigation card components
		$data['leftUserCard'] = $this->home_model->leftUserCard($_SESSION['u_id']);
		$data['overalls'] = $this->home_model->overallProgress($_SESSION['u_id']);
		// User recent activity card components
		$data['header'] = $this->home_model->getRecentActivity($_SESSION['u_id']);
		// sessions componets
		$data['topic_cards'] = $this->home_model->joinTopicAndProgress($_SESSION['u_id']);
		$data['worksheets'] = $this->home_model->getAllWorksheets($_SESSION['u_id']);
		$data['active_sess'] = $this->home_model->activeSession();
		$data['active_sess'] = ($data['active_sess'][0]['sheet_num']) ? $data['active_sess'][0]['sheet_num'] : 1;

		// All these data gets broken down to create information represented in the homepage
    $this->load->view('templates/header', $page);
    $this->load->view('pages/home-page', $data);
    $this->load->view('templates/footer');
	}
}
