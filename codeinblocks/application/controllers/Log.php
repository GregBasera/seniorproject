<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends CI_Controller {
	// This is where it all starts. This function sends the login/signup page
	// from the server to a client.
	public function index($f = 'l', $e = 0){
		$acceptable = array("l", "s");
    if (array_search($f, $acceptable) === FALSE) {
      show_404();
    }
		$page['title'] = "Login";
    $data['form_type'] = $f;
		$data['error'] = $e;

    $this->load->view('templates/header', $page);
    $this->load->view('pages/login-page', $data);
		$this->load->view('templates/footer');
	}

	// After the user has filled the login-page, the submit button will send the
	// inputed credentials from the client to the server. Those login credientials
	// is processed here. First, all the quotation marks (") are removed from the
	// strings as a step to prevent SQL injections. Then it is sent to a model. The
	// model returns information about the USER_PROFILE if it is valid, else it
	// return nothing. If the credientials are valid, the gathered information is
	// saved on a session and the user is redirected to his home_page, else an error
	// is prompted on the screen.
	public function in(){
		$creds = array(
			'username' => str_replace(['"', "'"], '', strval($this->input->post('username'))),
			'password' => str_replace(['"', "'"], '', strval(sha1($this->input->post('password'))))
		);

		$user_ID = $this->user_model->login($creds['username'], $creds['password']);

		if(!empty($user_ID)){
			$_SESSION['u_id'] = $user_ID[0]['user_ID'];
			$_SESSION['user'] = $creds['username'];
			$_SESSION['pass'] = $creds['password'];
			redirect('home', 'refresh');
		} else {
			redirect('log/index/l/1', 'refresh');
		}
	}

	// If the user decided to logout, the event is processed here. The user is redirected
	// to the login-page and the session is set to null.
	public function out(){
		$_SESSION['u_id'] = null;
		$_SESSION['user'] = null;
		$_SESSION['pass'] = null;
		redirect('log', 'refresh');
	}

	// If a new user signedup in the system, his/her signup form gets processed here. The
	// user is given a unique ID and the quotation marks is removed as a step to prevent
	// SQL injections. An activity is inserted in the database. Then it is sent to a model. The
	// model returns information about the USER_PROFILE if it is valid, else it
	// return nothing. If the credientials are valid, the gathered information is
	// saved on a session and the user is redirected to his home_page,
	public function signup(){
		$creds = array(
			'user_ID' => uniqid(),
			'fname' => str_replace(['"', "'"], '', strval(ucwords(strtolower($this->input->post('fname'))))),
			'lname' => str_replace(['"', "'"], '', strval(ucwords(strtolower($this->input->post('lname'))))),
			'username' => str_replace(['"', "'"], '', strval($this->input->post('username'))),
			'password' => str_replace(['"', "'"], '', strval(sha1($this->input->post('password')))),
			'email' => str_replace(['"', "'"], '', strval($this->input->post('email')))
		);

		$activity = array(
			'user_ID' => $creds['user_ID'],
			'activity_ID' => 'cib00000'
		);

		if($this->user_model->signup($creds, $activity) == true){
			$_SESSION['u_id'] = $creds['user_ID'];
			$_SESSION['user'] = $creds['username'];
			$_SESSION['pass'] = $creds['password'];

			redirect('home', 'refresh');
		}	else {
			// if signup has failed, it goes here. but this is highly unlikely :/
			echo "yah nah.. Signup failed..<br>please contact an admin..";
		}
	}
}
