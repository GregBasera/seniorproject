# PROJECT
---
| Name | CodeinBlock |
|---|---|
| Description | A Visual Programming Environment built to find out if a Visual Programming Library integrated in a granular discussion of programming concepts is an effective tool in teaching novice programmers |
| Authors | Greg Emerson A. Basera __(gbasera@gbox.adnu.edu.ph)__ |
|  | Nicohlus Ian R. Caneba __(ncaneba@gbox.adnu.edu.ph)__ |
|  | Janine Mae H. Llorca __(jmllorca@gbox.adnu.edu.ph)__ |
| Date Created | April 13, 2019 |
| Date Finished| ??? |

# Bugs and Design Flaws
---
1. Answers to Exercise Slides can be seen with a simple F12 on a browser. Copying these keys and pasting to, for instance, a print block would be incorrectly classified as a correct input.
2. Exercise slides can be skiped using the right url.
  * Easy fix: use the slide_IDs instead of slide_num
  * A verification mechanism would probably solve this.
3. Models can be better.
4. Rapidly pressing arrow keys on the slides can sometimes cause the exercise slide lock to not bite.
5. Progress attribute only changes via the nextButton, however, pressing prevButton a couple of times and then pressing nextButton once can render the progress attribute inaccurate.

# Setting-up LAMP stack on Ubuntu
---
### Apache
Install Apache HTTP server
```
  $ sudo apt-get update
  $ sudo apt-get upgrade
  $ sudo apt-get install apache2
```
Apache should start automatically upon installation. Type "localhost" on the adress bar of a browser -- you should see the home page of Apache.

### MySQL
Install MySQL server
```
  $ sudo apt-get install mysql-server
```
Sometimes the __root__ user is automatically cofigured during installation, this can cause confusion as to what the password for root is.

In my experience the command below usually works:
```
  $ sudo mysql -u root -p
```
you will be asked for your sudo password and the password for __root__.

Type in the same password for both.

You've successfully installed Mysql if you were able to login using the __root__ account.

### PHP
Install PHP and some libraries to help interface with Apache and MySQL.
```
  $ sudo apt-get install php libapache2-mod-php php-mysql
```

### Content
Clone this repository.
```
  $ git clone ...
```

__*Grats! You now have the ingredients for a LAMP stack. Lets now configure them to work together.*__

### Configure
In the DirectoryIndex move "index.php" so that its the first thing on the list.
```
  sudo nano /etc/apache2/mods-enabled/dir.conf
```

Change the root directory of Apache to where you cloned this repository.
```
  $ sudo nano /etc/apache2/sites-available/000-default.conf
  $ sudo nano /etc/apache2/apache2.conf
```

Restart Apache.
```
  $ sudo a2enmod rewrite
  $ sudo systemctl restart apache2
```

Configure Database. Create a MySQL user and a Database. For these project we used this commands.
```
  $ sudo mysql -u root -p
  > GRANT ALL PRIVILEGES ON *.* TO 'codeinblocks'@'localhost' IDENTIFIED BY 'blocks';
  > quit
  $ mysql -u codeinblock -p
  : blocks
```
Initialize the database.
```
  > source path/to/the/provided/sql/file
```
__*That should be it. idk. If you're having problems [this](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04) might help.*__

Export/Dump the database.
```
  $ mysqldump -u codeinblocks -p codeinblocks > [database name].sql
  pass: blocks
```
